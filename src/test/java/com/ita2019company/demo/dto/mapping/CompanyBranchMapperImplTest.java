package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.domain.CompanyBranch;
import com.ita2019company.demo.dto.AddressDto;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.EmployeeDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {AddressMapperImpl.class, CompanyBranchMapperImpl.class})
class CompanyBranchMapperImplTest {

    @Autowired
    private CompanyBranchMapper companyBranchMapper;

    private Address location;
    private CompanyDto companyDto;
    private AddressDto locationDto;
    private List<EmployeeDto> employees;
    private CompanyBranch testingEntity;
    private CompanyBranchDto testingDto;

    @BeforeEach
    void beforeEach() {
        location = new Address("city", "street", "houseNumber", "zipCode", "country");
        testingEntity = new CompanyBranch();
        locationDto = AddressDto.builder()
                .setCity("city")
                .setCountry("country")
                .setStreet("street")
                .setHouseNumber("houseNumber")
                .setZipCode("zipCode")
                .build();
        testingDto = CompanyBranchDto.builder()
                .setId(1L)
                .setName("Name")
                .setCompany(companyDto)
                .setEmployees(employees)
                .setLocation(locationDto)
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(companyBranchMapper.companyBranchToCompanyBranchDto(testingEntity)).isExactlyInstanceOf(CompanyBranchDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).isExactlyInstanceOf(CompanyBranch.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        CompanyBranchDto dto = companyBranchMapper.companyBranchToCompanyBranchDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "name",
                "id",
                "company",
                "employees");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        CompanyBranch entity = companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "name",
                "id",
                "company",
                "employees");
    }

    @Test
    void mapperToDtoCallAddressMapper() {
        testingEntity.setLocation(location);
        assertThat(companyBranchMapper.companyBranchToCompanyBranchDto(testingEntity).getLocation()).isInstanceOf(AddressDto.class).isNotNull();
    }

    @Test
    void mapperToEntityCallAddressMapper() {
        assertThat(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto).getLocation()).isInstanceOf(Address.class).isNotNull();
    }
}