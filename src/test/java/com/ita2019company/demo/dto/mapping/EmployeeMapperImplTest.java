package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Employee;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.ProjectDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {AddressMapperImpl.class, EmployeeMapperImpl.class})
class EmployeeMapperImplTest {

    @Autowired
    private EmployeeMapper employeeMapper;

    private CompanyBranchDto companyBranchDto;
    private List<ProjectDto> projectsDto;
    private Employee testingEntity;
    private EmployeeDto testingDto;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        testingEntity = new Employee();
        testingDto = EmployeeDto.builder()
                .setId(1L)
                .setSalary(10000L)
                .setFirstName("FirstName")
                .setSurname("Surname")
                .setCompanyBranch(companyBranchDto)
                .setProjects(projectsDto)
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(employeeMapper.employeeToEmployeeDto(testingEntity)).isExactlyInstanceOf(EmployeeDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(employeeMapper.employeeDtoToEmployee(testingDto)).isExactlyInstanceOf(Employee.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        EmployeeDto dto = employeeMapper.employeeToEmployeeDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "id",
                "firstName",
                "surname",
                "salary",
                "companyBranch",
                "projects");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        Employee entity = employeeMapper.employeeDtoToEmployee(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "id",
                "firstName",
                "surname",
                "salary",
                "companyBranch",
                "projects");
    }
}