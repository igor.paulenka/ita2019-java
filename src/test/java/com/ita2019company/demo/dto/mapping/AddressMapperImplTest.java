package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.dto.AddressDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = AddressMapperImpl.class)
class AddressMapperImplTest {

    @Autowired
    private AddressMapper addressMapper;

    private Address testingEntity;
    private AddressDto testingDto;

    @BeforeEach
    void beforeEach() {
        testingEntity = new Address("city", "street", "houseNumber", "zipCode", "country");
        testingDto = AddressDto.builder()
                .setCity("city")
                .setCountry("country")
                .setStreet("street")
                .setHouseNumber("houseNumber")
                .setZipCode("zipCode")
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(addressMapper.addressToAddressDto(testingEntity)).isExactlyInstanceOf(AddressDto.class).isNotNull();
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(addressMapper.addressDtoToAddress(testingDto)).isExactlyInstanceOf(Address.class).isNotNull();
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        AddressDto dto = addressMapper.addressToAddressDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingFieldByField(dto);
    }

    @Test
    void EntityHasSameValuesAsDto() {
        Address entity = addressMapper.addressDtoToAddress(testingDto);
        assertThat(entity).isEqualToComparingFieldByField(testingDto);
    }
}