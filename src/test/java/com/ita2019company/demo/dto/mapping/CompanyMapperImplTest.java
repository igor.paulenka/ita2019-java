package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.domain.CompanyBranch;
import com.ita2019company.demo.domain.Project;
import com.ita2019company.demo.dto.AddressDto;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.ProjectDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {AddressMapperImpl.class, CompanyMapperImpl.class})
class CompanyMapperImplTest {

    @Autowired
    private CompanyMapper companyMapper;

    private Address headquarters;
    private AddressDto headquartersDto;
    private List<CompanyBranch> companyBranches;
    private List<Project> projects;
    private List<CompanyBranchDto> companyBranchesDto;
    private List<ProjectDto> projectsDto;
    private Company testingEntity;
    private CompanyDto testingDto;

    @BeforeEach
    void beforeEach() {
        headquarters = new Address("city", "street", "houseNumber", "zipCode", "country");
        testingEntity = new Company(1L, "CompanyName", "0000000", "CZ00000000", "www.web.cz", "note", true, headquarters, companyBranches, projects);
        headquartersDto = AddressDto.builder()
                .setCity("city")
                .setCountry("country")
                .setStreet("street")
                .setHouseNumber("houseNumber")
                .setZipCode("zipCode")
                .build();
        testingDto = CompanyDto.builder()
                .setId(1L)
                .setName("CompanyName")
                .setCompanyId("0000000")
                .setVatId("CZ00000000")
                .setWebUrl("www.web.cz")
                .setNote("note")
                .setActive(true)
                .setHeadquarters(headquartersDto)
                .setCompanyBranches(companyBranchesDto)
                .setProjects(projectsDto)
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(companyMapper.companyToCompanyDto(testingEntity)).isExactlyInstanceOf(CompanyDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(companyMapper.companyDtoToCompany(testingDto)).isExactlyInstanceOf(Company.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        CompanyDto dto = companyMapper.companyToCompanyDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "name",
                "id",
                "companyId",
                "vatId",
                "webUrl",
                "note",
                "active",
                "companyBranches",
                "projects");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        Company entity = companyMapper.companyDtoToCompany(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "name",
                "id",
                "companyId",
                "vatId",
                "webUrl",
                "note",
                "active",
                "companyBranches",
                "projects");
    }

    @Test
    void mapperToDtoCallAddressMapper() {
        assertThat(companyMapper.companyToCompanyDto(testingEntity).getHeadquarters()).isInstanceOf(AddressDto.class).isNotNull();
    }

    @Test
    void mapperToEntityCallAddressMapper() {
        assertThat(companyMapper.companyDtoToCompany(testingDto).getHeadquarters()).isInstanceOf(Address.class).isNotNull();
    }
}