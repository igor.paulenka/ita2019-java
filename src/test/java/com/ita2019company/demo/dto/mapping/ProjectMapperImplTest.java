package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Project;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.ProjectDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {AddressMapperImpl.class, ProjectMapperImpl.class})
class ProjectMapperImplTest {

    @Autowired
    private ProjectMapper projectMapper;

    private CompanyDto companyDto;
    private List<EmployeeDto> employee;
    private Project testingEntity;
    private ProjectDto testingDto;

    @BeforeEach
    void beforeEach() {
        testingEntity = new Project();
        testingDto = ProjectDto.builder()
                .setCompany(companyDto)
                .setEmployees(employee)
                .setId(1L)
                .setName("name")
                .setPrice(1000L)
                .setStartDate("StartDate")
                .setEndDate("EndName")
                .build();
    }

    @Test
    void mapperConvertEntityToDto() {
        assertThat(projectMapper.projectToProjectDto(testingEntity)).isExactlyInstanceOf(ProjectDto.class);
    }

    @Test
    void mapperConvertDtoToEntity() {
        assertThat(projectMapper.projectDtoToProject(testingDto)).isExactlyInstanceOf(Project.class);
    }

    @Test
    void dtoHasSameValuesAsEntity() {
        ProjectDto dto = projectMapper.projectToProjectDto(testingEntity);
        assertThat(testingEntity).isEqualToComparingOnlyGivenFields(dto,
                "company",
                "id",
                "employees",
                "name",
                "price",
                "startDate",
                "endDate");
    }

    @Test
    void EntityHasSameValuesAsDto() {
        Project entity = projectMapper.projectDtoToProject(testingDto);
        assertThat(entity).isEqualToComparingOnlyGivenFields(testingDto,
                "company",
                "id",
                "employees",
                "name",
                "price",
                "startDate",
                "endDate");
    }
}