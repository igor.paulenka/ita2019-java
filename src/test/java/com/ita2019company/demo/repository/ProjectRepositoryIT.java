package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.domain.Project;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
public class ProjectRepositoryIT {

    private static final long PROJECT_ID = 1;
    private static final long NON_EXISTING_ID = 0;
    private static final long COMPANY_ID = 1;

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedProjects = 3;
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(returnedProjects);
    }

    @Test
    void findAllReturnsProjects() {
        final int returnedProjects = 3;
        final String project1 = "Table of sales creation";
        final String project2 = "Rebuilding entrance area";
        final String project3 = "Locker room upgrade";
        List<Project> projectList = projectRepository.findAll();
        assertThat(projectList).hasSize(returnedProjects);

        assertThat(projectList).satisfiesAnyOf(
                name -> assertThat(name.get(0).getName()).isEqualTo(project1),
                name -> assertThat(name.get(0).getName()).isEqualTo(project2),
                name -> assertThat(name.get(0).getName()).isEqualTo(project3));

        assertThat(projectList).satisfiesAnyOf(
                name -> assertThat(name.get(1).getName()).isEqualTo(project1),
                name -> assertThat(name.get(1).getName()).isEqualTo(project2),
                name -> assertThat(name.get(1).getName()).isEqualTo(project3));

        assertThat(projectList).satisfiesAnyOf(
                name -> assertThat(name.get(2).getName()).isEqualTo(project1),
                name -> assertThat(name.get(2).getName()).isEqualTo(project2),
                name -> assertThat(name.get(2).getName()).isEqualTo(project3));

        assertThat(projectList.get(0).getName()).isNotEqualTo(projectList.get(1).getName()).isNotEqualTo(projectList.get(2).getName());
        assertThat(projectList.get(0).getId()).isNotEqualTo(projectList.get(1).getId()).isNotEqualTo(projectList.get(2).getId()).isNotNull();
    }

    @Test
    void findAllByNameAndCreationTimeReturnsAll() {
        final int returnedProjects = 1;
        final String name = "Rebuild";
        final LocalDateTime createdAfter = LocalDateTime.now().minusYears(10);
        List<Project> projectList = projectRepository.findAll(name, createdAfter);
        assertThat(projectList).hasSize(returnedProjects);
    }

    @Test
    void findAllByNameAndCreationTimeReturnProject() {
        final int returnedProjects = 1;
        final LocalDateTime createdAfter = LocalDateTime.now().minusYears(10);
        final String name = "Table";
        final String projectName = "Table of sales creation";
        List<Project> projectList = projectRepository.findAll(name, createdAfter);
        assertThat(projectList).hasSize(returnedProjects);
        assertThat(projectList.get(0).getName()).isNotNull().isEqualTo(projectName);
        assertThat(projectList.get(0).getCreatedAt()).isNotNull().isAfter(createdAfter);
    }

    @Test
    void createReturnCompanyAndGenerateId() {
        Project project = createProject();
        Project createdProject = projectRepository.create(project);
        assertThat(createdProject.getId()).isNotNull();
        assertThat(createdProject.getName()).isEqualTo(createdProject.getName());
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        Project createdProject = new Project();
        createdProject.setName(null);
        assertThatThrownBy(() -> projectRepository.create(createdProject))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateProject() {
        Project oldProject = createProject();
        oldProject.setId(PROJECT_ID);
        Project newProject = projectRepository.update(oldProject);
        assertThat(newProject.getName()).isEqualTo(createProject().getName());
    }

    @Test
    void deleteByIdDeleteCompany() {
        List<Project> oldList = projectRepository.findAll();
        projectRepository.delete(oldList.get(0).getId());
        List<Project> newList = projectRepository.findAll();
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenCompanyDoNotExist() {
        assertThatThrownBy(() -> projectRepository.delete(NON_EXISTING_ID))
                .isExactlyInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    private Project createProject() {
        Project project = new Project();
        Company company = new Company();
        Address headquarters = new Address();
        headquarters.setHouseNumber("123").setStreet("street").setCity("city").setCountry("CZE").setZipCode("123456");
        company.setHeadquarters(headquarters).setName("name").setCompanyId("12345678").setId(COMPANY_ID);
        project.setCompany(company).setName("ProjectName");
        return project;
    }
}