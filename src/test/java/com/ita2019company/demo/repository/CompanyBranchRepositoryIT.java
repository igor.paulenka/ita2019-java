package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.domain.CompanyBranch;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
public class CompanyBranchRepositoryIT {

    private static final long COMPANY_BRANCH_ID = 2;
    private static final long COMPANY_ID = 1;
    private static final long NON_EXISTING_ID = 0;

    @Autowired
    private CompanyBranchRepository companyBranchRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedBranches = 2;
        List<CompanyBranch> companyList = companyBranchRepository.findAll(COMPANY_ID);
        assertThat(companyList).hasSize(returnedBranches);
    }

    @Test
    void findAllReturnsCompanyBranches() {
        final int returnedCompanyBranches = 2;
        final String branch1Name = "Hypermarket Brno - Modřice";
        final String branch2Name = "Supermarket Brno Rozkvět";
        List<CompanyBranch> companyBranches = companyBranchRepository.findAll(COMPANY_ID);
        assertThat(companyBranches).hasSize(returnedCompanyBranches);

        assertThat(companyBranches).satisfiesAnyOf(
                name -> assertThat(name.get(0).getName()).isEqualTo(branch1Name),
                name -> assertThat(name.get(0).getName()).isEqualTo(branch2Name));
        assertThat(companyBranches).satisfiesAnyOf(
                name -> assertThat(name.get(1).getName()).isEqualTo(branch1Name),
                name -> assertThat(name.get(1).getName()).isEqualTo(branch2Name));
        assertThat(companyBranches.get(0).getName()).isNotEqualTo(companyBranches.get(1).getName());
        assertThat(companyBranches.get(0).getId()).isNotEqualTo(companyBranches.get(1).getId()).isNotNull();
    }

    @Test
    void createReturnCompanyBranchAndGenerateId() {
        CompanyBranch companyBranch = createCompanyBranch();
        CompanyBranch createdCoBr = companyBranchRepository.create(companyBranch);
        assertThat(createdCoBr.getId()).isNotNull();
        assertThat(createdCoBr.getName()).isEqualTo(createCompanyBranch().getName());
        assertThat(createdCoBr.getLocation()).isEqualTo(companyBranch.getLocation());
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        CompanyBranch createdCoBr = new CompanyBranch();
        createdCoBr.setName("Supermarket Brno Rozkvět").setLocation(null);
        assertThatThrownBy(() -> companyBranchRepository.create(createdCoBr))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateCompanyBranch() {
        final int companyBranches = 2;
        CompanyBranch oldCoBr = createCompanyBranch();
        oldCoBr.setId(COMPANY_BRANCH_ID);
        CompanyBranch newCoBr = companyBranchRepository.update(oldCoBr);
        assertThat(companyBranchRepository.findAll(COMPANY_ID)).hasSize(companyBranches);
        assertThat(newCoBr.getName()).isEqualTo(createCompanyBranch().getName());
    }

    @Test
    void deleteByIdDeleteCompanyBranch() {
        CompanyBranch companyBranch = companyBranchRepository.create(createCompanyBranch());
        List<CompanyBranch> oldList = companyBranchRepository.findAll(COMPANY_ID);
        companyBranchRepository.delete(companyBranch.getId());
        List<CompanyBranch> newList = companyBranchRepository.findAll(COMPANY_ID);
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenCompanyBranchDoNotExist() {
        assertThatThrownBy(() -> companyBranchRepository.delete(NON_EXISTING_ID))
                .isExactlyInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    private CompanyBranch createCompanyBranch() {
        CompanyBranch companyBranch = new CompanyBranch();
        final Address location = new Address();
        final Company company = new Company();
        company.setId(COMPANY_ID);
        location.setHouseNumber("123").setStreet("street").setCity("city").setCountry("CZE").setZipCode("123456");
        companyBranch.setName("name").setLocation(location).setCompany(company);
        return companyBranch;
    }
}