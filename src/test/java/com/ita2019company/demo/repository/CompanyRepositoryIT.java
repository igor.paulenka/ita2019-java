package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.domain.Company;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
public class CompanyRepositoryIT {

    private static final long EXISTING_ID = 1;
    private static final long NON_EXISTING_ID = 0;

    @Autowired
    private CompanyRepository companyRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedCompanies = 2;
        final String company1 = "Albert Česká republika, s.r.o.";
        final String company2 = "BILLA, spol. s.r.o.";
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(returnedCompanies);

        assertThat(companyList).satisfiesAnyOf(
                name -> assertThat(name.get(0).getName()).isEqualTo(company1),
                name -> assertThat(name.get(0).getName()).isEqualTo(company2));
        assertThat(companyList).satisfiesAnyOf(
                name -> assertThat(name.get(1).getName()).isEqualTo(company1),
                name -> assertThat(name.get(1).getName()).isEqualTo(company2));
        assertThat(companyList.get(0).getName()).isNotEqualTo(companyList.get(1).getName());
        assertThat(companyList.get(0).getId()).isNotEqualTo(companyList.get(1).getId()).isNotNull();
    }

    @Test
    void findAllReturnsCompanyBranches() {
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList.get(0).getCompanyBranches()).isNotNull();
    }

    @Test
    void createSaveCompanyAndGenerateId() {
        Company newCompany = createCompany();
        Company createdCompany = companyRepository.save(newCompany);
        assertThat(createdCompany.getId()).isNotNull();
        assertThat(createdCompany.getName()).isEqualTo(newCompany.getName());
        assertThat(createdCompany.getCompanyId()).isEqualTo(newCompany.getCompanyId());
    }

    @Test
    void createThrowExceptionWhenDuplicatedData() {
        Company newCompany = new Company();
        Address headquarters = new Address().setHouseNumber("123").setStreet("street").setCity("city").setCountry("CZE").setZipCode("123456");
        newCompany.setName("Albert Česká republika, s.r.o.").setCompanyId("44012373").setHeadquarters(headquarters);
        assertThatThrownBy(() -> companyRepository.save(newCompany))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        Company newCompany = new Company();
        newCompany.setName("Albert Česká republika, s.r.o.").setCompanyId("44012373").setHeadquarters(null);
        assertThatThrownBy(() -> companyRepository.save(newCompany))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateCompany() {
        final int returnedCompanies = 2;
        Company oldCompany = createCompany();
        oldCompany.setId(EXISTING_ID);
        Company newCompany = companyRepository.save(oldCompany);
        assertThat(companyRepository.findAll()).hasSize(returnedCompanies);
        assertThat(newCompany.getName()).isEqualTo(createCompany().getName());
    }

    @Test
    void getOneReturnCompany() {
        Company company = companyRepository.getOne(EXISTING_ID);
        assertThat(company.getId()).isEqualTo(EXISTING_ID);
    }

    @Test
    void deleteByIdDeleteCompany() {
        Company company = companyRepository.save(createCompany());
        List<Company> oldList = companyRepository.findAll();
        companyRepository.deleteById(company.getId());
        List<Company> newList = companyRepository.findAll();
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenCompanyDoNotExist() {
        assertThatThrownBy(() -> companyRepository.deleteById(NON_EXISTING_ID))
                .isExactlyInstanceOf(EmptyResultDataAccessException.class);
    }

    private Company createCompany() {
        Company company = new Company();
        final Address headquarters = new Address();
        headquarters.setHouseNumber("123").setStreet("street").setCity("city").setCountry("CZE").setZipCode("123456");
        company.setName("name").setCompanyId("12345678").setVatId("CZ12345678").setHeadquarters(headquarters);
        return company;
    }
}