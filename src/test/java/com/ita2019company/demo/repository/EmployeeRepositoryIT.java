package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ActiveProfiles("it")
@SpringBootTest
@Transactional
public class EmployeeRepositoryIT {

    private static final long EMPLOYEE_ID = 1;
    private static final long NON_EXISTING_ID = 0;
    private static final long COMPANY_ID = 1;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    void findAllReturnsAll() {
        final int returnedEmployee = 4;
        List<Employee> employeeList = employeeRepository.findAll(COMPANY_ID);
        assertThat(employeeList).hasSize(returnedEmployee);
    }

    @Test
    void findAllReturnsEmployees() {
        final int returnedEmployee = 2;
        final long companyID = 2;
        final String surname1 = "Etinolák";
        final String surname2 = "Ferdinand";
        List<Employee> employeeList = employeeRepository.findAll(companyID);
        assertThat(employeeList).hasSize(returnedEmployee);

        assertThat(employeeList).satisfiesAnyOf(
                surname -> assertThat(surname.get(0).getSurname()).isEqualTo(surname1),
                surname -> assertThat(surname.get(0).getSurname()).isEqualTo(surname2));
        assertThat(employeeList).satisfiesAnyOf(
                surname -> assertThat(surname.get(1).getSurname()).isEqualTo(surname1),
                surname -> assertThat(surname.get(1).getSurname()).isEqualTo(surname2));
        assertThat(employeeList.get(0).getSurname()).isNotEqualTo(employeeList.get(1).getSurname());
        assertThat(employeeList.get(0).getFirstName()).isNotEqualTo(employeeList.get(1).getFirstName()).isNotNull();
        assertThat(employeeList.get(0).getId()).isNotEqualTo(employeeList.get(1).getId()).isNotNull();
    }

    @Test
    void createSaveReturnCompanyAndGenerateId() {
        Employee employee = createEmployee();
        Employee createdEmployee = employeeRepository.create(employee);
        assertThat(createdEmployee.getId()).isNotNull();
        assertThat(createdEmployee.getFirstName()).isEqualTo(createEmployee().getFirstName());
        assertThat(createdEmployee.getSurname()).isEqualTo(employee.getSurname());
    }

    @Test
    void createThrowExceptionWhenNotNullValuePolicyViolation() {
        Employee createdEmployee = new Employee();
        createdEmployee.setFirstName("FirstName").setSurname(null);
        assertThatThrownBy(() -> employeeRepository.create(createdEmployee))
                .isExactlyInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void updateMethodUpdateEmployee() {
        final int employees = employeeRepository.findAll(COMPANY_ID).size();
        Employee newEmployee = createEmployee();
        newEmployee.setId(EMPLOYEE_ID);
        Employee updatedEmployee = employeeRepository.update(newEmployee);
        assertThat(employeeRepository.findAll(COMPANY_ID)).hasSize(employees - 1);
        assertThat(updatedEmployee.getSurname()).isEqualTo(createEmployee().getSurname());
    }

    @Test
    void deleteByIdDeleteEmployee() {
        List<Employee> oldList = employeeRepository.findAll(COMPANY_ID);
        employeeRepository.delete(oldList.get(0).getId());
        List<Employee> newList = employeeRepository.findAll(COMPANY_ID);
        assertThat(newList).hasSize((oldList.size()) - 1);
    }

    @Test
    void deleteThrowExceptionWhenCompanyDoNotExist() {
        assertThatThrownBy(() -> employeeRepository.delete(NON_EXISTING_ID))
                .isExactlyInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    private Employee createEmployee() {
        Employee employee = new Employee();
        employee.setFirstName("FirstName").setSurname("Surname").setSalary(1000);
        return employee;
    }
}