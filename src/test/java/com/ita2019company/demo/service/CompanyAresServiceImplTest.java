package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.webservices.ares.AresRequestBuilder;
import com.ita2019company.demo.webservices.ares.CompanyAresMapper;
import cz.mfcr.ares.answer.AresOdpovedi;
import cz.mfcr.ares.request.AresDotazy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.xml.datatype.DatatypeConfigurationException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CompanyAresServiceImplTest {

    private CompanyAresService companyAresService;

    @Mock
    private WebServiceTemplate webServiceTemplate;
    @Mock
    private AresRequestBuilder aresRequestBuilder;
    @Mock
    private CompanyAresMapper companyAresMapper;
    @Mock
    private CompanyService companyService;

    private static final String ID = "12345678";

    private AresDotazy request;
    private AresOdpovedi response;

    @BeforeEach
    void beforeEach() throws DatatypeConfigurationException {
        MockitoAnnotations.initMocks(this);
        companyAresService = new CompanyAresServiceImpl(webServiceTemplate, aresRequestBuilder, companyAresMapper, companyService);
        request = new AresDotazy();
        response = new AresOdpovedi();
        CompanyDto dto = new CompanyDto.CompanyDtoBuilder().build();

        when(aresRequestBuilder.createRequest(ID)).thenReturn(request);
        when(webServiceTemplate.marshalSendAndReceive(request)).thenReturn(response);
        when(companyAresMapper.aresOdpovedToCompany(response)).thenReturn(dto);
        when(companyService.createCompany(dto)).thenReturn(dto);
    }

    @Test
    void callCreate() throws DatatypeConfigurationException {
        companyAresService.findByCompanyId(ID);
        verify(companyService, times(1)).createCompany(any());
    }

    @Test
    void callMapper() throws DatatypeConfigurationException {
        companyAresService.findByCompanyId(ID);
        verify(companyAresMapper, times(1)).aresOdpovedToCompany(response);
    }

    @Test
    void returnCompanyDto() throws DatatypeConfigurationException {
        assertThat(companyAresService.findByCompanyId(ID)).isInstanceOf(CompanyDto.class);
    }
}