package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.Employee;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.mapping.EmployeeMapper;
import com.ita2019company.demo.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class EmployeeServiceImplTest {

    private EmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;
    @Mock
    private EmployeeMapper employeeMapper;

    private Employee testingEntity;
    private EmployeeDto testingDto;
    private static long ID = 1;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        employeeService = new EmployeeServiceImpl(employeeRepository, employeeMapper);
        testingEntity = new Employee();
        testingDto = EmployeeDto.builder().build();
        Mockito.validateMockitoUsage();
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(employeeRepository.findAll(ID)).thenReturn(Arrays.asList(testingEntity));
        employeeService.findAll(ID);
        verify(employeeRepository, times(1)).findAll(ID);
    }

    @Test
    void findAllCallMapperOnce() {
        when(employeeMapper.employeeToEmployeeDto(testingEntity)).thenReturn(testingDto);
        when(employeeRepository.findAll(ID)).thenReturn(Arrays.asList(testingEntity));
        employeeService.findAll(ID);
        verify(employeeMapper, times(1)).employeeToEmployeeDto(testingEntity);
    }

    @Test
    void findAllIsTriggered_thenEntityIsFind() {
        employeeService.findAll(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(employeeRepository).findAll(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }

    @Test
    void createCallRepository() {
        when(employeeMapper.employeeDtoToEmployee(testingDto)).thenReturn(testingEntity);
        when(employeeRepository.create(testingEntity)).thenReturn(testingEntity);
        employeeService.create(testingDto);
        verify(employeeRepository, times(1)).create(testingEntity);
    }

    @Test
    void createCallMapper() {
        when(employeeMapper.employeeDtoToEmployee(testingDto)).thenReturn(testingEntity);
        when(employeeRepository.create(testingEntity)).thenReturn(testingEntity);
        employeeService.create(testingDto);
        verify(employeeMapper, times(1)).employeeToEmployeeDto(testingEntity);
        verify(employeeMapper, times(1)).employeeDtoToEmployee(testingDto);
    }

    @Test
    void createIsTriggered_thenEntityIsCreated() {
        when(employeeMapper.employeeDtoToEmployee(testingDto)).thenReturn(testingEntity);
        employeeService.create(testingDto);

        ArgumentCaptor<Employee> argument = ArgumentCaptor.forClass(Employee.class);
        verify(employeeRepository).create(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Employee.class);
    }

    @Test
    void updateCallRepository() {
        when(employeeMapper.employeeDtoToEmployee(testingDto)).thenReturn(testingEntity);
        when(employeeRepository.update(testingEntity)).thenReturn(testingEntity);
        employeeService.update(testingDto);
        verify(employeeRepository, times(1)).update(testingEntity);
    }

    @Test
    void updateCallMapper() {
        when(employeeMapper.employeeDtoToEmployee(testingDto)).thenReturn(testingEntity);
        when(employeeRepository.update(testingEntity)).thenReturn(testingEntity);
        employeeService.update(testingDto);
        verify(employeeMapper, times(1)).employeeDtoToEmployee(testingDto);
    }

    @Test
    void updateIsTriggered_thenEntityIsUpdate() {
        when(employeeMapper.employeeDtoToEmployee(testingDto)).thenReturn(testingEntity);
        employeeService.update(testingDto);

        ArgumentCaptor<Employee> argument = ArgumentCaptor.forClass(Employee.class);
        verify(employeeRepository).update(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Employee.class);
    }

    @Test
    void deleteCallRepository() {
        employeeService.delete(ID);
        verify(employeeRepository, times(1)).delete(ID);
    }

    @Test
    void deleteIsTriggered_thenEntityIsDeleted() {
        employeeService.delete(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(employeeRepository).delete(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }
}