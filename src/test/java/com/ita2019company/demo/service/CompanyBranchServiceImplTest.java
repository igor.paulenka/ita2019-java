package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.CompanyBranch;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.mapping.CompanyBranchMapper;
import com.ita2019company.demo.repository.CompanyBranchRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class CompanyBranchServiceImplTest {

    private CompanyBranchService companyBranchService;

    @Mock
    private CompanyBranchRepository companyBranchRepository;
    @Mock
    private CompanyBranchMapper companyBranchMapper;

    private CompanyBranch testingEntity;
    private CompanyBranchDto testingDto;
    private static long ID = 1;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        companyBranchService = new CompanyBranchServiceImpl(companyBranchRepository, companyBranchMapper);
        testingEntity = new CompanyBranch();
        testingDto = CompanyBranchDto.builder().build();
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(companyBranchRepository.findAll(ID)).thenReturn(Arrays.asList(testingEntity));
        companyBranchService.findAll(ID);
        verify(companyBranchRepository, times(1)).findAll(ID);
    }

    @Test
    void findAllCallMapperOnce() {
        when(companyBranchMapper.companyBranchToCompanyBranchDto(testingEntity)).thenReturn(testingDto);
        when(companyBranchRepository.findAll(ID)).thenReturn(Arrays.asList(testingEntity));
        companyBranchService.findAll(ID);
        verify(companyBranchMapper, times(1)).companyBranchToCompanyBranchDto(testingEntity);
    }

    @Test
    void findAllIsTriggered_thenEntityIsFind() {
        companyBranchService.findAll(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(companyBranchRepository).findAll(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }

    @Test
    void createCallRepository() {
        when(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).thenReturn(testingEntity);
        when(companyBranchRepository.create(testingEntity)).thenReturn(testingEntity);
        companyBranchService.create(testingDto);
        verify(companyBranchRepository, times(1)).create(testingEntity);
    }

    @Test
    void createCallMapper() {
        when(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).thenReturn(testingEntity);
        when(companyBranchRepository.create(testingEntity)).thenReturn(testingEntity);
        companyBranchService.create(testingDto);
        verify(companyBranchMapper, times(1)).companyBranchToCompanyBranchDto(testingEntity);
        verify(companyBranchMapper, times(1)).companyBranchDtoToCompanyBranch(testingDto);
    }

    @Test
    void createIsTriggered_thenEntityIsCreated() {
        when(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).thenReturn(testingEntity);
        companyBranchService.create(testingDto);

        ArgumentCaptor<CompanyBranch> argument = ArgumentCaptor.forClass(CompanyBranch.class);
        verify(companyBranchRepository).create(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(CompanyBranch.class);
    }

    @Test
    void updateCallRepository() {
        when(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).thenReturn(testingEntity);
        when(companyBranchRepository.update(testingEntity)).thenReturn(testingEntity);
        companyBranchService.update(testingDto);
        verify(companyBranchRepository, times(1)).update(testingEntity);
    }

    @Test
    void updateCallMapper() {
        when(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).thenReturn(testingEntity);
        when(companyBranchRepository.update(testingEntity)).thenReturn(testingEntity);
        companyBranchService.update(testingDto);
        verify(companyBranchMapper, times(1)).companyBranchDtoToCompanyBranch(testingDto);
    }

    @Test
    void updateIsTriggered_thenEntityIsUpdate() {
        when(companyBranchMapper.companyBranchDtoToCompanyBranch(testingDto)).thenReturn(testingEntity);
        companyBranchService.update(testingDto);

        ArgumentCaptor<CompanyBranch> argument = ArgumentCaptor.forClass(CompanyBranch.class);
        verify(companyBranchRepository).update(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(CompanyBranch.class);
    }

    @Test
    void deleteCallRepository() {
        companyBranchService.delete(ID);
        verify(companyBranchRepository, times(1)).delete(ID);
    }

    @Test
    void deleteIsTriggered_thenEntityIsDeleted() {
        companyBranchService.delete(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(companyBranchRepository).delete(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }
}