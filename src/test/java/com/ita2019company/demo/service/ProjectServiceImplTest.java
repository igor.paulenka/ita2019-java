package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.Project;
import com.ita2019company.demo.dto.ProjectDto;
import com.ita2019company.demo.dto.mapping.ProjectMapper;
import com.ita2019company.demo.repository.ProjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class ProjectServiceImplTest {

    private ProjectService projectService;
    @Mock
    private ProjectRepository projectRepository;
    @Mock
    private ProjectMapper projectMapper;

    private Project testingEntity;
    private ProjectDto testingDto;
    private LocalDateTime timestamp;
    private static final long ID = 1;
    private static final String NAME = "name";

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        projectService = new ProjectServiceImpl(projectRepository, projectMapper);
        testingEntity = new Project();
        testingDto = ProjectDto.builder().build();
        Clock clock = Clock.fixed(Instant.parse("2019-11-25T10:15:30.00Z"), ZoneId.of("UTC"));
        timestamp = LocalDateTime.now(clock);
    }

    @Test
    void findAllWithArgsCallRepositoryOnce() {
        when(projectRepository.findAll(NAME, timestamp)).thenReturn(Arrays.asList(testingEntity));
        projectService.findAll(NAME, timestamp);
        verify(projectRepository, times(1)).findAll(NAME, timestamp);
    }

    @Test
    void findAllWithArgsCallMapperOnce() {
        when(projectMapper.projectToProjectDto(testingEntity)).thenReturn(testingDto);
        when(projectRepository.findAll(NAME, timestamp)).thenReturn(Arrays.asList(testingEntity));
        projectService.findAll(NAME, timestamp);
        verify(projectMapper, times(1)).projectToProjectDto(testingEntity);
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(projectRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        projectService.findAll();
        verify(projectRepository, times(1)).findAll();
    }

    @Test
    void findAllCallMapperOnce() {
        when(projectMapper.projectToProjectDto(testingEntity)).thenReturn(testingDto);
        when(projectRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        projectService.findAll();
        verify(projectMapper, times(1)).projectToProjectDto(testingEntity);
    }

    @Test
    void createCallRepository() {
        when(projectMapper.projectDtoToProject(testingDto)).thenReturn(testingEntity);
        when(projectRepository.create(testingEntity)).thenReturn(testingEntity);
        projectService.create(testingDto);
        verify(projectRepository, times(1)).create(testingEntity);
    }

    @Test
    void createCallMapper() {
        when(projectMapper.projectDtoToProject(testingDto)).thenReturn(testingEntity);
        when(projectRepository.create(testingEntity)).thenReturn(testingEntity);
        projectService.create(testingDto);
        verify(projectMapper, times(1)).projectToProjectDto(testingEntity);
        verify(projectMapper, times(1)).projectDtoToProject(testingDto);
    }

    @Test
    void createIsTriggered_thenEntityIsCreated() {
        when(projectMapper.projectDtoToProject(testingDto)).thenReturn(testingEntity);
        projectService.create(testingDto);

        ArgumentCaptor<Project> argument = ArgumentCaptor.forClass(Project.class);
        verify(projectRepository).create(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Project.class);
    }

    @Test
    void updateCallRepository() {
        when(projectMapper.projectDtoToProject(testingDto)).thenReturn(testingEntity);
        when(projectRepository.update(testingEntity)).thenReturn(testingEntity);
        projectService.update(testingDto);
        verify(projectRepository, times(1)).update(testingEntity);
    }

    @Test
    void updateCallMapper() {
        when(projectMapper.projectDtoToProject(testingDto)).thenReturn(testingEntity);
        when(projectRepository.update(testingEntity)).thenReturn(testingEntity);
        projectService.update(testingDto);
        verify(projectMapper, times(1)).projectDtoToProject(testingDto);
    }

    @Test
    void updateIsTriggered_thenEntityIsUpdate() {
        when(projectMapper.projectDtoToProject(testingDto)).thenReturn(testingEntity);
        projectService.update(testingDto);

        ArgumentCaptor<Project> argument = ArgumentCaptor.forClass(Project.class);
        verify(projectRepository).update(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Project.class);
    }

    @Test
    void deleteCallRepository() {
        projectService.delete(ID);
        verify(projectRepository, times(1)).delete(ID);
    }

    @Test
    void deleteIsTriggered_thenEntityIsDeleted() {
        projectService.delete(ID);
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(projectRepository).delete(argument.capture());
        assertThat(argument.getValue()).isEqualTo(ID);
    }
}