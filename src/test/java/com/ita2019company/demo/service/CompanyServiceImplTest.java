package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.mapping.CompanyMapper;
import com.ita2019company.demo.exception.RecordNotFoundException;
import com.ita2019company.demo.repository.CompanyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;

import java.sql.SQLException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class CompanyServiceImplTest {

    private CompanyService companyService;
    private Company testingEntity;
    private CompanyDto testingDto;
    @Mock
    private CompanyRepository companyRepository;
    @Mock
    private CompanyMapper companyMapper;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        companyService = new CompanyServiceImpl(companyRepository, companyMapper);
        testingEntity = new Company();
        testingDto = CompanyDto.builder().build();
    }

    @Test
    void findAllCallRepositoryOnce() {
        when(companyRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        companyService.findAll();
        verify(companyRepository, times(1)).findAll();
    }

    @Test
    void findAllCallMapperOnce() {
        when(companyMapper.companyToCompanyDto(testingEntity)).thenReturn(testingDto);
        when(companyRepository.findAll()).thenReturn(Arrays.asList(testingEntity));
        companyService.findAll();
        verify(companyMapper, times(1)).companyToCompanyDto(testingEntity);
    }

    @Test
    void createCompanyCallRepository() {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        when(companyRepository.save(testingEntity)).thenReturn(testingEntity);
        companyService.createCompany(testingDto);
        verify(companyRepository, times(1)).save(testingEntity);
    }

    @Test
    void createCompanyCallMapper() {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        when(companyRepository.save(testingEntity)).thenReturn(testingEntity);
        companyService.createCompany(testingDto);
        verify(companyMapper, times(1)).companyToCompanyDto(testingEntity);
        verify(companyMapper, times(1)).companyDtoToCompany(testingDto);
    }

    @Test
    void createCompanyIsTriggered_thenEntityIsCreated() {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        companyService.createCompany(testingDto);

        ArgumentCaptor<Company> argument = ArgumentCaptor.forClass(Company.class);
        verify(companyRepository).save(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Company.class);
    }

    @Test
    void CreateCompanyCatchAndException() {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        doThrow(DataIntegrityViolationException.class).when(companyRepository).save(testingEntity);
        assertThatThrownBy(() -> companyService.createCompany(testingDto))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining(String.format("Duplicate values was inserted."));
    }

    @Test
    void updateCompanyCallRepository() throws SQLException {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        when(companyRepository.getOne(testingEntity.getId())).thenReturn(testingEntity);
        when(companyRepository.save(testingEntity)).thenReturn(testingEntity);
        companyService.updateCompany(testingDto);
        verify(companyRepository, times(1)).save(testingEntity);
        verify(companyRepository, times(1)).getOne(testingEntity.getId());
    }

    @Test
    void updateCompanyCallMapper() throws SQLException {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        when(companyRepository.getOne(testingEntity.getId())).thenReturn(testingEntity);
        when(companyRepository.save(testingEntity)).thenReturn(testingEntity);
        companyService.updateCompany(testingDto);
        verify(companyMapper, times(1)).companyDtoToCompany(testingDto);
    }

    @Test
    void updateCompanyIsTriggered_thenEntityIsUpdate() throws SQLException {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        when(companyRepository.getOne(testingEntity.getId())).thenReturn(testingEntity);
        companyService.updateCompany(testingDto);

        ArgumentCaptor<Company> argument = ArgumentCaptor.forClass(Company.class);
        verify(companyRepository).save(argument.capture());
        assertThat(argument.getValue()).isEqualToComparingFieldByField(testingEntity);
        assertThat(argument.getValue()).isInstanceOf(Company.class);
    }

    @Test
    void updateCompanyThrowJpaObjectRetrievalFailureExceptionException2() {
        when(companyMapper.companyDtoToCompany(testingDto)).thenReturn(testingEntity);
        when(companyRepository.getOne(testingEntity.getId())).thenReturn(testingEntity);
        doThrow(JpaObjectRetrievalFailureException.class).when(companyRepository).save(testingEntity);
        assertThatThrownBy(() -> companyService.updateCompany(testingDto))
                .isExactlyInstanceOf(RecordNotFoundException.class)
                .hasMessageContaining(String.format("Company with Id '%s' not found", testingDto.getId()));
    }

    @Test
    void deleteCompanyCallRepository() {
        companyService.deleteCompany(anyLong());
        verify(companyRepository, times(1)).deleteById(anyLong());
    }

    @Test
    void deleteCompanyIsTriggered_thenEntityIsDeleted() {
        companyService.deleteCompany(anyLong());
        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(companyRepository).deleteById(argument.capture());
        assertThat(argument.getValue()).isInstanceOf(Long.class);
    }

    @Test
    void deleteCompanyCatchAndException() {
        doThrow(EmptyResultDataAccessException.class).when(companyRepository).deleteById(anyLong());
        assertThatThrownBy(() -> companyService.deleteCompany(anyLong()))
                .isExactlyInstanceOf(RecordNotFoundException.class);
    }
}