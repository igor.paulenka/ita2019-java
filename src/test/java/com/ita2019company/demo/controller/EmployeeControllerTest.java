package com.ita2019company.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    private static final long COMPANY_ID = 1;
    private static final long COMPANY_BRANCH_ID = 2;
    private static final long EMPLOYEE_ID = 3;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private EmployeeService employeeService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        EmployeeDto dto = createDto();
        when(employeeService.findAll(COMPANY_ID)).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(
                get("/employee")
                        .param("companyId", String.valueOf(COMPANY_ID)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id")
                        .value(dto.getId()))
                .andExpect(jsonPath("$[0].companyBranch.id")
                        .value(dto.getCompanyBranch().getId()))
                .andExpect(jsonPath("$[0].companyBranch.company.id")
                        .value(dto.getCompanyBranch().getCompany().getId()));
        verify(employeeService, times(1)).findAll(COMPANY_ID);
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        EmployeeDto dto = createDto();
        mockMvc.perform(
                put("/employee")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(employeeService, times(1)).update(any(EmployeeDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        EmployeeDto dto = createDto();
        mockMvc.perform(
                post("/employee")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(employeeService, times(1)).create(any(EmployeeDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/employee")
                        .param("id", String.valueOf(EMPLOYEE_ID)))
                .andExpect(status().isNoContent());
        verify(employeeService, times(1)).delete(eq(EMPLOYEE_ID));
    }

    private EmployeeDto createDto() {
        CompanyDto companyDto = CompanyDto.builder()
                .setId(COMPANY_ID)
                .build();
        CompanyBranchDto companyBranchDto = CompanyBranchDto.builder()
                .setId(COMPANY_BRANCH_ID)
                .setCompany(companyDto)
                .build();
        return EmployeeDto.builder()
                .setId(EMPLOYEE_ID)
                .setCompanyBranch(companyBranchDto)
                .build();
    }
}