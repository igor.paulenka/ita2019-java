package com.ita2019company.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.service.CompanyBranchService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CompanyBranchController.class)
class CompanyBranchControllerTest {

    private static final long COMPANY_BRANCH_ID = 1;
    private static final long COMPANY_ID = 2;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CompanyBranchService companyBranchService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        CompanyBranchDto dto = createDto();
        when(companyBranchService.findAll(COMPANY_ID)).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(
                get("/companybranch")
                        .param("companyId", String.valueOf(COMPANY_ID)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(dto.getId()))
                .andExpect(jsonPath("$[0].company.id").value(dto.getCompany().getId()));
        verify(companyBranchService, times(1)).findAll(COMPANY_ID);
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        CompanyBranchDto dto = createDto();
        mockMvc.perform(
                put("/companybranch")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(companyBranchService, times(1)).update(any(CompanyBranchDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        CompanyBranchDto dto = createDto();
        mockMvc.perform(
                post("/companybranch")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(companyBranchService, times(1)).create(any(CompanyBranchDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/companybranch")
                        .param("id", String.valueOf(COMPANY_BRANCH_ID)))
                .andExpect(status().isNoContent());
        verify(companyBranchService, times(1)).delete(eq(COMPANY_BRANCH_ID));
    }

    private CompanyBranchDto createDto() {
        CompanyDto companyDto = CompanyDto.builder()
                .setId(COMPANY_ID)
                .build();
        return CompanyBranchDto.builder()
                .setId(COMPANY_BRANCH_ID)
                .setCompany(companyDto)
                .build();
    }
}