package com.ita2019company.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.service.CompanyAresService;
import com.ita2019company.demo.service.CompanyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CompanyController.class)
class CompanyControllerTest {

    private static final long ID = 1;
    private static final String COMPANY_ID = "12345678";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CompanyService companyService;
    @MockBean
    private CompanyAresService companyAresService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        CompanyDto dto = createDto();
        when(companyService.findAll()).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(get("/company"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(dto.getId()));
        verify(companyService, times(1)).findAll();
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        CompanyDto dto = createDto();
        mockMvc.perform(
                put("/company")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(companyService, times(1)).updateCompany(any(CompanyDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        CompanyDto dto = createDto();
        mockMvc.perform(
                post("/company")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(companyService, times(1)).createCompany(any(CompanyDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/company")
                        .param("id", String.valueOf(ID)))
                .andExpect(status().isNoContent());
        verify(companyService, times(1)).deleteCompany(eq(ID));
    }

    @Test
    void getTriggeredThenCompanyAresServiceIsCalled() throws Exception {
        CompanyDto dto = createAresDto();
        when(companyAresService.findByCompanyId(COMPANY_ID)).thenReturn(dto);
        mockMvc.perform(
                get("/company/ares/{companyId}", COMPANY_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(dto.getId()))
                .andExpect(jsonPath("$.companyId").value(COMPANY_ID));
        verify(companyAresService, times(1)).findByCompanyId(COMPANY_ID);
    }

    private CompanyDto createDto() {
        return CompanyDto.builder()
                .setId(ID)
                .build();
    }

    private CompanyDto createAresDto() {
        return CompanyDto.builder()
                .setId(ID)
                .setCompanyId(COMPANY_ID)
                .build();
    }
}