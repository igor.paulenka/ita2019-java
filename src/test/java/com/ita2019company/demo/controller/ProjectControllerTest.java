package com.ita2019company.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.ProjectDto;
import com.ita2019company.demo.service.ProjectService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProjectController.class)
class ProjectControllerTest {

    private static final long COMPANY_ID = 1;
    private static final long PROJECT_ID = 2;
    private static final long EMPLOYEE_ID = 3;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ProjectService projectService;

    @Test
    void getTriggeredThenServiceIsCalled() throws Exception {
        ProjectDto dto = createDto();
        when(projectService.findAll()).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(get("/project"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id")
                        .value(dto.getId()))
                .andExpect(jsonPath("$[0].employees.[0].id")
                        .value(dto.getEmployees().get(0).getId()))
                .andExpect(jsonPath("$[0].company.id")
                        .value(dto.getCompany().getId()));
        verify(projectService, times(1)).findAll();
    }

    @Test
    void getWithArgsTriggeredThenServiceIsCalled() throws Exception {
        ProjectDto dto = createDto();
        final LocalDateTime createdAfter = LocalDateTime.now();
        final String substringOfProjectName = "SubstringOfName";
        when(projectService.findAll(substringOfProjectName, createdAfter)).thenReturn(Collections.singletonList(dto));
        mockMvc.perform(
                get("/project")
                        .param("name", substringOfProjectName)
                        .param("createdAfter", String.valueOf(createdAfter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id")
                        .value(dto.getId()))
                .andExpect(jsonPath("$[0].employees.[0].id")
                        .value(dto.getEmployees().get(0).getId()))
                .andExpect(jsonPath("$[0].company.id")
                        .value(dto.getCompany().getId()));
        verify(projectService, times(1)).findAll(substringOfProjectName, createdAfter);
    }

    @Test
    void putTriggeredThenServiceIsCalled() throws Exception {
        ProjectDto dto = createDto();
        mockMvc.perform(
                put("/project")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        verify(projectService, times(1)).update(any(ProjectDto.class));
    }

    @Test
    void postTriggeredThenServiceIsCalled() throws Exception {
        ProjectDto dto = createDto();
        mockMvc.perform(
                post("/project")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        verify(projectService, times(1)).create(any(ProjectDto.class));
    }

    @Test
    void deleteTriggeredThenServiceIsCalled() throws Exception {
        mockMvc.perform(
                delete("/project")
                        .param("id", String.valueOf(PROJECT_ID)))
                .andExpect(status().isNoContent());
        verify(projectService, times(1)).delete(eq(PROJECT_ID));
    }

    private ProjectDto createDto() {
        CompanyDto companyDto = CompanyDto.builder()
                .setId(COMPANY_ID)
                .build();
        EmployeeDto employeeDto = EmployeeDto.builder()
                .setId(EMPLOYEE_ID)
                .build();
        return ProjectDto.builder()
                .setId(PROJECT_ID)
                .setCompany(companyDto)
                .setEmployees(Collections.singletonList(employeeDto))
                .build();
    }
}