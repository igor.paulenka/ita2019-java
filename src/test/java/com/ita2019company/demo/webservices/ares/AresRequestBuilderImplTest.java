package com.ita2019company.demo.webservices.ares;

import cz.mfcr.ares.request.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;

class AresRequestBuilderImplTest {

    private AresRequestBuilder aresRequestBuilder;

    @Value("${user.mail}")
    private String ARES_BY_ICO_MAIL;

    private static final String ID = "12345678";
    private static final String INVALID_ID = "1234567";
    private static final String EXCEPTION_MESSAGE_SUBSTRING = "must contain sequence of 8 digits";

    private static final AresDotazTyp ARES_BY_ICO_TYPE = AresDotazTyp.STANDARD;
    private static final String ARES_BY_ICO_ID = "ares_dotaz";
    private static final String ARES_BY_ICO_VALIDATION_XSLT = "http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_request/v_1.0.0/ares_request.xsl";
    private static final String ARES_BY_ICO_ANSWER_NAMESPACE_REQUIRED = "http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_answer/v_1.0.1";
    private static final int ARES_BY_ICO_NUMBER_OF_QUERIES = 1;
    private static final AresVyberTyp ARES_BY_ICO_TYPE_OF_SEARCH = AresVyberTyp.FREE;
    private static final int ARES_BY_ICO_MAX_COUNT = 10;
    private static final int ARES_BY_ICO_SUBID = 1;
    private static final VystupFormat ARES_BY_IBO_OUTPUT_FORMAT = VystupFormat.XML;

    @BeforeEach
    void beforeEach() {
        aresRequestBuilder = new AresRequestBuilderImpl();
    }

    @Test
    void createRequestCreateAresDotaz() throws DatatypeConfigurationException {
        assertThat(aresRequestBuilder.createRequest(ID)).isNotNull().isInstanceOf(AresDotazy.class);
    }

    @Test
    void createRequestCatchAndThrowException() {
        assertThatThrownBy(() -> aresRequestBuilder.createRequest(INVALID_ID))
                .isExactlyInstanceOf(IllegalArgumentException.class).hasMessageContaining(EXCEPTION_MESSAGE_SUBSTRING);
    }

    @Test
    void createRequestHasProperAresDotazi() throws DatatypeConfigurationException {
        assertThat(aresRequestBuilder.createRequest(ID).getDotazTyp()).hasFieldOrProperty(String.valueOf(ARES_BY_ICO_TYPE));
        assertThat(aresRequestBuilder.createRequest(ID).getDotazPocet()).hasToString(String.valueOf(ARES_BY_ICO_NUMBER_OF_QUERIES));
        assertThat(aresRequestBuilder.createRequest(ID).getAnswerNamespaceRequired()).isEqualTo(ARES_BY_ICO_ANSWER_NAMESPACE_REQUIRED);
        assertThat(aresRequestBuilder.createRequest(ID).getId()).isEqualTo(ARES_BY_ICO_ID);
        assertThat(aresRequestBuilder.createRequest(ID).getValidationXSLT()).isEqualTo(ARES_BY_ICO_VALIDATION_XSLT);
        assertThat(aresRequestBuilder.createRequest(ID).getUserMail()).isEqualTo(ARES_BY_ICO_MAIL);
        assertThat(aresRequestBuilder.createRequest(ID).getDotazDatumCas()).isInstanceOf(XMLGregorianCalendar.class);
        assertThat(aresRequestBuilder.createRequest(ID).getVystupFormat()).hasFieldOrProperty(String.valueOf(ARES_BY_IBO_OUTPUT_FORMAT));
        assertThat(aresRequestBuilder.createRequest(ID).getDotaz()).isInstanceOf(ArrayList.class).doesNotContainNull();
    }

    @Test
    void createRequestHasProperDotaz() throws DatatypeConfigurationException {
        assertThat(aresRequestBuilder.createRequest(ID).getDotaz().get(0).getTypVyhledani()).hasToString(String.valueOf(ARES_BY_ICO_TYPE_OF_SEARCH));
        assertThat(aresRequestBuilder.createRequest(ID).getDotaz().get(0).getKlicovePolozky()).isInstanceOf(KlicovePolozky.class);
        assertThat(aresRequestBuilder.createRequest(ID).getDotaz().get(0).getPomocneID()).hasToString(String.valueOf(ARES_BY_ICO_SUBID));
        assertThat(aresRequestBuilder.createRequest(ID).getDotaz().get(0).getMaxPocet()).hasToString(String.valueOf(ARES_BY_ICO_MAX_COUNT));
    }

    @Test
    void createRequestHasKlicovePolozky() throws DatatypeConfigurationException {
        assertThat(aresRequestBuilder.createRequest(ID).getDotaz().get(0).getKlicovePolozky().getICO()).isEqualTo(ID);
    }
}