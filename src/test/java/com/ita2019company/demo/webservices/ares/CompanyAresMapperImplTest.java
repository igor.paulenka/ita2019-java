package com.ita2019company.demo.webservices.ares;

import com.ita2019company.demo.dto.CompanyDto;
import cz.mfcr.ares.answer.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {CompanyAresMapperImpl.class})
class CompanyAresMapperImplTest {

    @Autowired
    @Spy
    CompanyAresMapper companyAresMapper;

    private AresOdpovedi response;

    private static final String ID = "12345678";
    private static final String ANY_STRING = "AAAAAAAAAAA";
    private static final int ANY_INT = 1;
    private static final String CZE = "203";

    protected AresOdpovedi responseBuild() {
        response = new AresOdpovedi();
        Odpoved odpoved = new Odpoved();
        response.getOdpoved().add(0, odpoved);
        Zaznam zaznam = new Zaznam();
        odpoved.getZaznam().add(0, zaznam);
        Identifikace identifikace = new Identifikace();
        zaznam.setIdentifikace(identifikace);
        AdresaARES adresaARES = new AdresaARES();
        identifikace.setAdresaARES(adresaARES);

        zaznam.setICO(ID);
        zaznam.setObchodniFirma(ANY_STRING);
        zaznam.setPriznakySubjektu(ANY_STRING);

        adresaARES.setPSC(ANY_STRING);
        adresaARES.setKodStatu(CZE);
        adresaARES.setNazevObce(ANY_STRING);
        adresaARES.setNazevCastiObce(ANY_STRING);
        adresaARES.setNazevMestskeCasti(ANY_STRING);
        adresaARES.setNazevUlice(ANY_STRING);
        adresaARES.setCisloDomovni(ANY_INT);
        adresaARES.setCisloOrientacni(ANY_STRING);
        return response;
    }

    @BeforeEach
    void beforeEach() {
        response = responseBuild();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void responseBuildProperlyWorkCheck() {
        assertThat(response).isInstanceOf(AresOdpovedi.class);
    }

    @Test
    void mapperReturnCompanyDto() {
        assertThat(companyAresMapper.aresOdpovedToCompany(response)).isInstanceOf(CompanyDto.class);
    }

    @Test
    void mapperReturnCompanyDtoWithProperID() {
        CompanyDto dto = companyAresMapper.aresOdpovedToCompany(response);
        assertThat(dto.getCompanyId()).isSameAs(ID);
    }

    @Test
    void mapperCallGenerateCountry() {
        companyAresMapper.aresOdpovedToCompany(response);
        verify(companyAresMapper, times(1)).generateCountry(CZE);
    }

    @Test
    void mapperGenerateCountry() {
        assertThat(companyAresMapper.generateCountry(CZE)).isEqualTo("CZE");
    }

    @Test
    void mapperDoesNotGenerateCountry() {
        assertThat(companyAresMapper.generateCountry(String.valueOf(ANY_INT))).isNotEqualTo("CZE");
    }

    @Test
    void mapperCallGenerateCity() {
        companyAresMapper.aresOdpovedToCompany(response);
        verify(companyAresMapper, times(1)).generateCity(ANY_STRING, ANY_STRING, ANY_STRING);
    }

    @Test
    void mapperCallGenerateHouseNumber() {
        CompanyDto dto = companyAresMapper.aresOdpovedToCompany(response);
        verify(companyAresMapper, times(1)).generateHouseNumber(ANY_INT, ANY_STRING);
    }

    @Test
    void generateHouseNumberReturnProperValue() {
        CompanyDto dto = companyAresMapper.aresOdpovedToCompany(response);
        assertThat(dto.getHeadquarters().getHouseNumber()).isEqualTo(ANY_INT + "/" + ANY_STRING);
    }

    @Test
    void mapperCallGenerateStatus() {
        companyAresMapper.aresOdpovedToCompany(response);
        verify(companyAresMapper, times(1)).getStatus(ANY_STRING);
    }

    @Test
    void generateStatusReturnProperStatus() {
        CompanyDto dto = companyAresMapper.aresOdpovedToCompany(response);
        assertThat(dto.isActive()).isEqualTo(true);
    }

    @Test
    void generateCityReturnProperValues() {
        CompanyDto dto = companyAresMapper.aresOdpovedToCompany(response);
        assertThat(dto.getHeadquarters().getCity()).isEqualTo(ANY_STRING + " - " + ANY_STRING + " - " + ANY_STRING);
    }
}