package com.ita2019company.demo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class AresServiceConfig {

    @Value("${ares.uri}")
    private String URI;

    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setDefaultUri(URI);
        webServiceTemplate.setMarshaller(requestMarshaller());
        webServiceTemplate.setUnmarshaller(answerMarshaller());
        return webServiceTemplate;
    }

    @Bean
    public Jaxb2Marshaller requestMarshaller() {
        Jaxb2Marshaller requestMarshaller = new Jaxb2Marshaller();
        requestMarshaller.setContextPath("cz.mfcr.ares.request");
        return requestMarshaller;
    }

    @Bean
    public Jaxb2Marshaller answerMarshaller() {
        Jaxb2Marshaller answerMarshaller = new Jaxb2Marshaller();
        answerMarshaller.setContextPath("cz.mfcr.ares.answer");
        return answerMarshaller;
    }
}