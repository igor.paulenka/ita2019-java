package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.CompanyBranch;

import java.util.List;

/**
 * Repository interface with declaration of methods which serves the CompanyBranch class.
 */
public interface CompanyBranchRepository {

    /**
     * Method find all company branches that meet inserted conditions.
     *
     * @param companyId Identification parameter (primary key) for company. Must not be null or negative.
     * @return List of all company branches that satisfy the specified requirement.
     */
    List<CompanyBranch> findAll(long companyId);

    /**
     * Method creates new company branch.
     *
     * @param companyBranch The company branch to be created.
     * @return Created company branch.
     */
    CompanyBranch create(CompanyBranch companyBranch);

    /**
     * Method updates existing company branch.
     *
     * @param companyBranch The company branch to be updated.
     * @return Updated company branch.
     */
    CompanyBranch update(CompanyBranch companyBranch);

    /**
     * Method deletes company branch chosen by company branches id.
     *
     * @param id Identification parameter (primary key) for company branch. Must not be null or negative.
     */
    void delete(long id);
}