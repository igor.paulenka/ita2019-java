package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Company;

import java.util.List;


/**
 * Repository interface with declaration of methods which serves the Company class.
 */
public interface CompanyRepository {

    /**
     * Method find all companies.
     *
     * @return List of all companies sorted alphabetically by company name.
     */
    List<Company> findAll();

    /**
     * Method create new or update old company.
     *
     * @param company Company that would be created or updated to.
     * @return Created or updated company.
     */
    Company save(Company company);

    /**
     * Method delete company chosen by company id.
     *
     * @param id identification parameter for companies. Must not be null or negative.
     */
    void deleteById(long id);

    /**
     * Method find company.
     *
     * @param id identification parameter for companies. Must not be null or negative.
     * @return Company with inserted id.
     */
    Company getOne(long id);
}