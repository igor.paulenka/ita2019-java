package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Employee> findAll(long companyId) {
        return entityManager.createNamedQuery(Employee.QUERY_ALL, Employee.class)
                .setParameter(1, companyId)
                .getResultList();
    }

    @Override
    public Employee create(Employee employee) {
        entityManager.persist(employee);
        return employee;
    }

    @Override
    public Employee update(Employee employee) {
        return entityManager.merge(employee);
    }

    @Override
    public void delete(long id) {
        Employee deletedEmployee = entityManager.find(Employee.class, id);
        entityManager.remove(deletedEmployee);
    }
}