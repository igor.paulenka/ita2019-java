package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.CompanyBranch;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CompanyBranchRepositoryImpl implements CompanyBranchRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<CompanyBranch> findAll(long companyId) {
        return entityManager.createNamedQuery(CompanyBranch.QUERY_ALL, CompanyBranch.class)
                .setParameter(1, companyId)
                .getResultList();
    }

    @Override
    public CompanyBranch create(CompanyBranch companyBranch) {
        entityManager.persist(companyBranch);
        return companyBranch;
    }

    @Override
    public CompanyBranch update(CompanyBranch companyBranch) {
        return entityManager.merge(companyBranch);
    }

    @Override
    public void delete(long id) {
        CompanyBranch deletedCompanyBranch = entityManager.find(CompanyBranch.class, id);
        entityManager.remove(deletedCompanyBranch);
    }
}