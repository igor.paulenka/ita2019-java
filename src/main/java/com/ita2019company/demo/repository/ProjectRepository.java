package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Project;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository interface with declaration of methods which serves the Project class.
 */
public interface ProjectRepository {

    /**
     * Method find all projects.
     *
     * @return List of all projects.
     */
    List<Project> findAll();

    /**
     * Method find all projects that meet inserted conditions.
     *
     * @param name         Only project with name that contained this substring will be selected.
     * @param createdAfter Only project that was create after this inserted date and time will be selected.
     * @return List of projects that satisfy all the specified requirement.
     */
    List<Project> findAll(String name, LocalDateTime createdAfter);

    /**
     * Method creates new project.
     *
     * @param project The project to be created.
     * @return Created project.
     */
    Project create(Project project);

    /**
     * Method updates existing project.
     *
     * @param project The project to be updated.
     * @return Updated project.
     */
    Project update(Project project);

    /**
     * Method deletes project chosen by projects id.
     *
     * @param id Identification parameter (primary key) for projects. Must not be null or negative.
     */
    void delete(long id);
}