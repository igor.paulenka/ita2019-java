package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository which serves the Company class.
 */
public interface CompanyJpaRepository extends JpaRepository<Company, Long>, CompanyRepository {
}
