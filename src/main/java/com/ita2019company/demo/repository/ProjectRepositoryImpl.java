package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Project;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Project> findAll() {
        Query query = entityManager.createNamedQuery(Project.QUERY_ALL, Project.class);
        return (List<Project>) query.getResultList();
    }

    @Override
    public List<Project> findAll(String name, LocalDateTime createdAfter) {
        return entityManager.createNamedQuery(Project.QUERY_ALL_NAME_CREATION_TIME, Project.class)
                .setParameter(1, "%" + name + "%")
                .setParameter(2, createdAfter)
                .getResultList();
    }

    @Override
    public Project create(Project project) {
        entityManager.persist(project);
        return project;
    }

    @Override
    public Project update(Project project) {
        entityManager.merge(project);
        return project;
    }

    @Override
    public void delete(long id) {
        Project deletedProject = entityManager.find(Project.class, id);
        entityManager.remove(deletedProject);
    }
}