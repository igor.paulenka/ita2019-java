package com.ita2019company.demo.repository;

import com.ita2019company.demo.domain.Employee;

import java.util.List;

/**
 * Repository interface with declaration of methods which serves the Employee class.
 */
public interface EmployeeRepository {

    /**
     * Method find all employees that meet inserted conditions.
     *
     * @param companyId Identification parameter (primary key) for company. Must not be null or negative.
     * @return List of all employees that satisfy the specified requirement.
     */
    List<Employee> findAll(long companyId);

    /**
     * Method creates new employee.
     *
     * @param employee The employee to be created.
     * @return Created employee.
     */
    Employee create(Employee employee);

    /**
     * Method updates existing employee.
     *
     * @param employee The employee to be updated.
     * @return Updated employee.
     */
    Employee update(Employee employee);

    /**
     * Method deletes employee chosen by employees id.
     *
     * @param id Identification parameter (primary key) for employee. Must not be null or negative.
     */
    void delete(long id);
}