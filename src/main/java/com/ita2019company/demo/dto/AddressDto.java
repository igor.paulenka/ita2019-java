package com.ita2019company.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = AddressDto.AddressDtoBuilder.class)
public class AddressDto {

    private String city;
    private String street;
    private String houseNumber;
    private String zipCode;
    private String country;

    private AddressDto(String city, String street, String houseNumber, String zipCode, String country) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.zipCode = zipCode;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountry() {
        return country;
    }

    public static AddressDtoBuilder builder() {
        return new AddressDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class AddressDtoBuilder {

        private String city;
        private String street;
        private String houseNumber;
        private String zipCode;
        private String country;

        public AddressDtoBuilder setCity(String city) {
            this.city = city;
            return this;
        }

        public AddressDtoBuilder setStreet(String street) {
            this.street = street;
            return this;
        }

        public AddressDtoBuilder setHouseNumber(String houseNumber) {
            this.houseNumber = houseNumber;
            return this;
        }

        public AddressDtoBuilder setZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public AddressDtoBuilder setCountry(String country) {
            this.country = country;
            return this;
        }

        public AddressDto build() {
            return new AddressDto(city, street, houseNumber, zipCode, country);
        }
    }
}