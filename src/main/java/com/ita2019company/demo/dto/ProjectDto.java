package com.ita2019company.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = ProjectDto.ProjectDtoBuilder.class)
public class ProjectDto extends AbstractDto {

    private final String name;
    private final String startDate;
    private final String endDate;
    private final long price;
    private final CompanyDto company;
    private final List<EmployeeDto> employees;

    private ProjectDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, String name, String startDate, String endDate, long price, CompanyDto company, List<EmployeeDto> employees) {
        super(id, createdAt, updatedAt);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.company = company;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public long getPrice() {
        return price;
    }

    public CompanyDto getCompany() {
        return company;
    }

    public List<EmployeeDto> getEmployees() {
        return employees;
    }

    public static ProjectDtoBuilder builder() {
        return new ProjectDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class ProjectDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String name;
        private String startDate;
        private String endDate;
        private long price;
        private CompanyDto company;
        private List<EmployeeDto> employees;

        public ProjectDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public ProjectDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public ProjectDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public ProjectDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public ProjectDtoBuilder setStartDate(String startDate) {
            this.startDate = startDate;
            return this;
        }

        public ProjectDtoBuilder setEndDate(String endDate) {
            this.endDate = endDate;
            return this;
        }

        public ProjectDtoBuilder setPrice(long price) {
            this.price = price;
            return this;
        }

        public ProjectDtoBuilder setCompany(CompanyDto company) {
            this.company = company;
            return this;
        }

        public ProjectDtoBuilder setEmployees(List<EmployeeDto> employees) {
            this.employees = employees;
            return this;
        }

        public ProjectDto build() {
            return new ProjectDto(id, createdAt, updatedAt, name, startDate, endDate, price, company, employees);
        }
    }
}