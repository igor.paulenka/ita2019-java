package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.domain.Employee;
import com.ita2019company.demo.domain.Project;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.ProjectDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = AddressMapper.class)
public interface ProjectMapper {

    ProjectDto projectToProjectDto(Project project);

    Project projectDtoToProject(ProjectDto project);

    @Mappings({
            @Mapping(target = "companyBranches", ignore = true),
            @Mapping(target = "projects", ignore = true),
    })
    CompanyDto companyToCompanyDto(Company company);

    @Mappings({
            @Mapping(target = "companyBranches", ignore = true),
            @Mapping(target = "projects", ignore = true),
    })
    Company companyDtoToCompany(CompanyDto company);

    @Mappings({
            @Mapping(target = "companyBranch", ignore = true),
            @Mapping(target = "projects", ignore = true)
    })
    EmployeeDto employeeToEmployeeDto(Employee employee);

    @Mappings({
            @Mapping(target = "companyBranch", ignore = true),
            @Mapping(target = "projects", ignore = true)
    })
    Employee employeeDtoToEmployee(EmployeeDto employee);
}