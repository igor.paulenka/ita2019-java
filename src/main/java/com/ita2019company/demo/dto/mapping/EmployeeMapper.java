package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.domain.CompanyBranch;
import com.ita2019company.demo.domain.Employee;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.EmployeeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(uses = AddressMapper.class, componentModel = "spring")
public interface EmployeeMapper {

    @Mappings(
            @Mapping(target = "projects", ignore = true))
    EmployeeDto employeeToEmployeeDto(Employee employee);

    @Mappings(
            @Mapping(target = "projects", ignore = true))
    Employee employeeDtoToEmployee(EmployeeDto employee);

    @Mappings(
            @Mapping(target = "employees", ignore = true)
    )
    CompanyBranchDto companyBranchToCompanyBranchDto(CompanyBranch companyBranch);

    @Mappings(
            @Mapping(target = "employees", ignore = true)
    )
    CompanyBranch companyBranchDtoToCompanyBranch(CompanyBranchDto companyBranch);

    @Mappings({
            @Mapping(target = "companyBranches", ignore = true),
            @Mapping(target = "projects", ignore = true)
    })
    CompanyDto companyToCompanyDto(Company company);

    @Mappings({
            @Mapping(target = "companyBranches", ignore = true),
            @Mapping(target = "projects", ignore = true)
    })
    Company companyDtoToCompany(CompanyDto company);
}