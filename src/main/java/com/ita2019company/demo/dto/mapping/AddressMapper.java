package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Address;
import com.ita2019company.demo.dto.AddressDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressDto addressToAddressDto(Address address);

    Address addressDtoToAddress(AddressDto address);
}