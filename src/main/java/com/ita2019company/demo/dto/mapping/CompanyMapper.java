package com.ita2019company.demo.dto.mapping;

import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.domain.CompanyBranch;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.CompanyDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(uses = AddressMapper.class, componentModel = "spring")
public interface CompanyMapper {

    @Mappings(
            @Mapping(target = "projects", ignore = true))
    CompanyDto companyToCompanyDto(Company company);

    @Mappings(
            @Mapping(target = "projects", ignore = true))
    Company companyDtoToCompany(CompanyDto company);

    @Mappings({
            @Mapping(target = "employees", ignore = true),
            @Mapping(target = "company", ignore = true),
    })
    CompanyBranchDto companyBranchToCompanyBranchDto(CompanyBranch companyBranch);

    @Mappings({
            @Mapping(target = "employees", ignore = true),
            @Mapping(target = "company", ignore = true)
    })
    CompanyBranch companyBranchDtoToCompanyBranch(CompanyBranchDto companyBranch);
}