package com.ita2019company.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = CompanyBranchDto.CompanyBranchDtoBuilder.class)
public class CompanyBranchDto extends AbstractDto {

    private final String name;
    private final AddressDto location;
    private final List<EmployeeDto> employees;
    private final CompanyDto company;

    private CompanyBranchDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, String name, AddressDto location, List<EmployeeDto> employees, CompanyDto company) {
        super(id, createdAt, updatedAt);
        this.name = name;
        this.location = location;
        this.employees = employees;
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public AddressDto getLocation() {
        return location;
    }

    public List<EmployeeDto> getEmployees() {
        return employees;
    }

    public CompanyDto getCompany() {
        return company;
    }

    public static CompanyBranchDtoBuilder builder() {
        return new CompanyBranchDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class CompanyBranchDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String name;
        private AddressDto location;
        private List<EmployeeDto> employees;
        private CompanyDto company;

        public CompanyBranchDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public CompanyBranchDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public CompanyBranchDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public CompanyBranchDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CompanyBranchDtoBuilder setLocation(AddressDto location) {
            this.location = location;
            return this;
        }

        public CompanyBranchDtoBuilder setEmployees(List<EmployeeDto> employees) {
            this.employees = employees;
            return this;
        }

        public CompanyBranchDtoBuilder setCompany(CompanyDto company) {
            this.company = company;
            return this;
        }

        public CompanyBranchDto build() {
            return new CompanyBranchDto(id, createdAt, updatedAt, name, location, employees, company);
        }
    }
}
