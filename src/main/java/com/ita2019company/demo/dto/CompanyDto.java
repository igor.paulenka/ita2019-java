package com.ita2019company.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = CompanyDto.CompanyDtoBuilder.class)
public class CompanyDto extends AbstractDto {

    private final String name;
    private final String companyId;
    private final String vatId;
    private final String webUrl;
    private final String note;
    private final boolean active;
    private final AddressDto headquarters;
    private final List<CompanyBranchDto> companyBranches;
    private final List<ProjectDto> projects;

    private CompanyDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, String name, String companyId, String vatId, String webUrl, String note, boolean active, AddressDto headquarters, List<CompanyBranchDto> companyBranches, List<ProjectDto> projects) {
        super(id, createdAt, updatedAt);
        this.name = name;
        this.companyId = companyId;
        this.vatId = vatId;
        this.webUrl = webUrl;
        this.note = note;
        this.active = active;
        this.headquarters = headquarters;
        this.companyBranches = companyBranches;
        this.projects = projects;
    }

    public String getName() {
        return name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getNote() {
        return note;
    }

    public boolean isActive() {
        return active;
    }

    public AddressDto getHeadquarters() {
        return headquarters;
    }

    public List<CompanyBranchDto> getCompanyBranches() {
        return companyBranches;
    }

    public List<ProjectDto> getProjects() {
        return projects;
    }

    public static CompanyDtoBuilder builder() {
        return new CompanyDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class CompanyDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String name;
        private String companyId;
        private String vatId;
        private String webUrl;
        private String note;
        private boolean active;
        private AddressDto headquarters;
        private List<CompanyBranchDto> companyBranches;
        private List<ProjectDto> projects;

        public CompanyDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public CompanyDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public CompanyDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public CompanyDtoBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CompanyDtoBuilder setCompanyId(String companyId) {
            this.companyId = companyId;
            return this;
        }

        public CompanyDtoBuilder setVatId(String vatId) {
            this.vatId = vatId;
            return this;
        }

        public CompanyDtoBuilder setWebUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public CompanyDtoBuilder setNote(String note) {
            this.note = note;
            return this;
        }

        public CompanyDtoBuilder setActive(boolean active) {
            this.active = active;
            return this;
        }

        public CompanyDtoBuilder setHeadquarters(AddressDto headquarters) {
            this.headquarters = headquarters;
            return this;
        }

        public CompanyDtoBuilder setCompanyBranches(List<CompanyBranchDto> companyBranches) {
            this.companyBranches = companyBranches;
            return this;
        }

        public CompanyDtoBuilder setProjects(List<ProjectDto> projects) {
            this.projects = projects;
            return this;
        }

        public CompanyDto build() {
            return new CompanyDto(id, createdAt, updatedAt, name, companyId, vatId, webUrl, note, active, headquarters, companyBranches, projects);
        }
    }
}