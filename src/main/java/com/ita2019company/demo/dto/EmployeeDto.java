package com.ita2019company.demo.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.List;

@JsonDeserialize(builder = EmployeeDto.EmployeeDtoBuilder.class)
public class EmployeeDto extends AbstractDto {

    private final String firstName;
    private final String surname;
    private final long salary;
    private final CompanyBranchDto companyBranch;
    private final List<ProjectDto> projects;

    private EmployeeDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt, String firstName, String surname, long salary, CompanyBranchDto companyBranch, List<ProjectDto> projects) {
        super(id, createdAt, updatedAt);
        this.firstName = firstName;
        this.surname = surname;
        this.salary = salary;
        this.companyBranch = companyBranch;
        this.projects = projects;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public long getSalary() {
        return salary;
    }

    public CompanyBranchDto getCompanyBranch() {
        return companyBranch;
    }

    public List<ProjectDto> getProjects() {
        return projects;
    }

    public static EmployeeDtoBuilder builder() {
        return new EmployeeDtoBuilder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class EmployeeDtoBuilder {

        private long id;
        private LocalDateTime createdAt;
        private LocalDateTime updatedAt;
        private String firstName;
        private String surname;
        private long salary;
        private CompanyBranchDto companyBranch;
        private List<ProjectDto> projects;

        public EmployeeDtoBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public EmployeeDtoBuilder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public EmployeeDtoBuilder setUpdatedAt(LocalDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        public EmployeeDtoBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeDtoBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public EmployeeDtoBuilder setSalary(long salary) {
            this.salary = salary;
            return this;
        }

        public EmployeeDtoBuilder setCompanyBranch(CompanyBranchDto companyBranch) {
            this.companyBranch = companyBranch;
            return this;
        }

        public EmployeeDtoBuilder setProjects(List<ProjectDto> projects) {
            this.projects = projects;
            return this;
        }

        public EmployeeDto build() {
            return new EmployeeDto(id, createdAt, updatedAt, firstName, surname, salary, companyBranch, projects);
        }
    }
}