package com.ita2019company.demo.dto;

import java.time.LocalDateTime;

public abstract class AbstractDto {

    private final long id;
    private final LocalDateTime createdAt;
    private final LocalDateTime updatedAt;

    public long getId() {
        return id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    AbstractDto(long id, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}