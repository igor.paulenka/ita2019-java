package com.ita2019company.demo.webservices.ares;

import com.ita2019company.demo.dto.AddressDto;
import com.ita2019company.demo.dto.CompanyDto;
import cz.mfcr.ares.answer.AresOdpovedi;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public abstract class CompanyAresMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "name", expression = "java(response.getOdpoved().get(0).getZaznam().get(0).getObchodniFirma())"),
            @Mapping(target = "companyId", expression = "java(response.getOdpoved().get(0).getZaznam().get(0).getICO())"),
            @Mapping(target = "active", expression = "java(getStatus(response.getOdpoved().get(0).getZaznam().get(0).getPriznakySubjektu()))"),
            @Mapping(target = "headquarters", expression = "java(adresaAresToAddress(response))")
    })
    public abstract CompanyDto aresOdpovedToCompany(AresOdpovedi response);

    @Mappings({
            @Mapping(target = "city", expression = "java(generateCity(response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getNazevObce(), response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getNazevCastiObce(), response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getNazevMestskeCasti()))"),
            @Mapping(target = "street", expression = "java(response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getNazevUlice())"),
            @Mapping(target = "houseNumber", expression = "java(generateHouseNumber(response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getCisloDomovni(), response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getCisloOrientacni()))"),
            @Mapping(target = "country", expression = "java(generateCountry(response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getKodStatu()))"),
            @Mapping(target = "zipCode", expression = "java(response.getOdpoved().get(0).getZaznam().get(0).getIdentifikace().getAdresaARES().getPSC())"),
    })
    protected abstract AddressDto adresaAresToAddress(AresOdpovedi response);

    protected String generateCountry(String state) {
        final String country = "CZE";
        final String notFound = "N/A";
        if (state.equals("203")) {
            return country;
        } else {
            return notFound;
        }
    }

    protected String generateCity(String Nazev_obce, String Nazev_casti_obce, String Nazev_mestske_casti) {
        if (Nazev_casti_obce == null) {
            return Nazev_obce;
        }
        if (Nazev_mestske_casti == null) {
            return Nazev_obce + " - " + Nazev_casti_obce;
        } else {
            return Nazev_obce + " - " + Nazev_casti_obce + " - " + Nazev_mestske_casti;
        }
    }

    protected String generateHouseNumber(int Cislo_domovni, String Cislo_orientacni) {
        if (Cislo_orientacni == null) {
            return String.valueOf(Cislo_domovni);
        } else {
            return Cislo_domovni + "/" + Cislo_orientacni;
        }
    }

    protected boolean getStatus(String active) {
        char status = active.charAt(1);
        return String.valueOf(status).equals("A");
    }
}