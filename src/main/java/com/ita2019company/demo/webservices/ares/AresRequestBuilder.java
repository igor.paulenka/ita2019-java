package com.ita2019company.demo.webservices.ares;

import cz.mfcr.ares.request.AresDotazy;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * Webservice interface with declaration of method which serve for requesting response from Ares webservice.
 */
public interface AresRequestBuilder {

    /**
     * Method create request for company from ARES webservice.
     *
     * @param companyId Identification parameter for companies (ICO). Must not be negative and must contain sequence of 8 digits.
     * @throws DatatypeConfigurationException
     */
    AresDotazy createRequest(String companyId) throws DatatypeConfigurationException;
}
