package com.ita2019company.demo.webservices.ares;

import cz.mfcr.ares.request.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;

@Component
public class AresRequestBuilderImpl implements AresRequestBuilder {

    @Value("${user.mail}")
    private String ARES_BY_ICO_MAIL;

    private static final AresDotazTyp ARES_BY_ICO_TYPE = AresDotazTyp.STANDARD;
    private static final String ARES_BY_ICO_ID = "ares_dotaz";
    private static final String ARES_BY_ICO_VALIDATION_XSLT = "http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_request/v_1.0.0/ares_request.xsl";
    private static final String ARES_BY_ICO_ANSWER_NAMESPACE_REQUIRED = "http://wwwinfo.mfcr.cz/ares/xml_doc/schemas/ares/ares_answer/v_1.0.1";
    private static final int ARES_BY_ICO_NUMBER_OF_QUERIES = 1;
    private static final AresVyberTyp ARES_BY_ICO_TYPE_OF_SEARCH = AresVyberTyp.FREE;
    private static final int ARES_BY_ICO_MAX_COUNT = 10;
    private static final int ARES_BY_ICO_SUBID = 1;
    private static final VystupFormat ARES_BY_IBO_OUTPUT_FORMAT = VystupFormat.XML;

    public AresDotazy createRequest(String companyId) throws DatatypeConfigurationException {
        if (companyId.length() != 8) {
            throw new IllegalArgumentException(String.format("CompanyId: '%s' is invalid, must contain sequence of 8 digits", companyId));
        } else {
            AresDotazy request = new AresDotazy();
            XMLGregorianCalendar timestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(LocalDateTime.now().toString());

            final String ARES_BY_ICO_ICO = companyId;
            KlicovePolozky polozky = new KlicovePolozky();
            polozky.setICO(ARES_BY_ICO_ICO);

            Dotaz newDotaz = new Dotaz();
            newDotaz.setTypVyhledani(ARES_BY_ICO_TYPE_OF_SEARCH);
            newDotaz.setMaxPocet(ARES_BY_ICO_MAX_COUNT);
            newDotaz.setPomocneID(ARES_BY_ICO_SUBID);
            newDotaz.setKlicovePolozky(polozky);

            request.setDotazDatumCas(timestamp);
            request.setDotazPocet(ARES_BY_ICO_NUMBER_OF_QUERIES);
            request.setDotazTyp(ARES_BY_ICO_TYPE);
            request.setUserMail(ARES_BY_ICO_MAIL);
            request.setId(ARES_BY_ICO_ID);
            request.setValidationXSLT(ARES_BY_ICO_VALIDATION_XSLT);
            request.setAnswerNamespaceRequired(ARES_BY_ICO_ANSWER_NAMESPACE_REQUIRED);
            request.setVystupFormat(ARES_BY_IBO_OUTPUT_FORMAT);
            request.getDotaz().add(newDotaz);

            return request;
        }
    }
}