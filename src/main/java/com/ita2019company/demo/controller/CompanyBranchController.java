package com.ita2019company.demo.controller;

import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.service.CompanyBranchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companybranch")
public class CompanyBranchController {

    private final CompanyBranchService companyBranchService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyBranchController.class);

    public CompanyBranchController(CompanyBranchService companyBranchService) {
        this.companyBranchService = companyBranchService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CompanyBranchDto> findAll(@RequestParam("companyId") long companyId) {
        LOGGER.info("Endpoint find all company branches of company with ID: {} was called.", companyId);
        List<CompanyBranchDto> companyBranchDtoList = companyBranchService.findAll(companyId);
        LOGGER.info("Returned was {} company branches.", companyBranchDtoList.size());
        return companyBranchDtoList;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public CompanyBranchDto updateCompanyBranch(@RequestBody CompanyBranchDto companyBranch) {
        LOGGER.info("Endpoint update company branch was called on company branch with ID: {}.", companyBranch.getId());
        CompanyBranchDto newCompanyBranch = companyBranchService.update(companyBranch);
        LOGGER.info("Company branch with ID: {} was updated to {}.", companyBranch.getId(), newCompanyBranch);
        return newCompanyBranch;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyBranchDto createCompanyBranch(@RequestBody CompanyBranchDto companyBranch) {
        LOGGER.info("Endpoint create companyBranch was called.");
        CompanyBranchDto companyBranchDto = companyBranchService.create(companyBranch);
        LOGGER.info("CompanyBranch {} was created.", companyBranchDto);
        return companyBranchDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompanyBranch(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete companyBranch with ID: {} was called.", id);
        companyBranchService.delete(id);
        LOGGER.info("CompanyBranch with ID: {} was deleted.", id);
    }
}