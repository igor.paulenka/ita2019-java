package com.ita2019company.demo.controller;

import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.service.CompanyAresService;
import com.ita2019company.demo.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.xml.datatype.DatatypeConfigurationException;
import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    private final CompanyService companyService;
    private final CompanyAresService companyAresService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

    public CompanyController(CompanyService companyService, CompanyAresService companyAresService) {
        this.companyService = companyService;
        this.companyAresService = companyAresService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CompanyDto> findAll() {
        LOGGER.info("Endpoint find all companies was called.");
        List<CompanyDto> companyDtoList = companyService.findAll();
        LOGGER.info("Returned was {} companies.", companyDtoList.size());
        return companyDtoList;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public CompanyDto updateCompany(@RequestBody CompanyDto company) {
        LOGGER.info("Endpoint update company was called on company with ID: {}.", company.getId());
        CompanyDto companyDto = companyService.updateCompany(company);
        LOGGER.info("Company with ID: {} was updated to {}.", company.getId(), company);
        return companyDto;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyDto createCompany(@RequestBody CompanyDto company) {
        LOGGER.info("Endpoint create company was called.");
        CompanyDto companyDto = companyService.createCompany(company);
        LOGGER.info("Company {} was created.", companyDto);
        return companyDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete company with ID: {} was called.", id);
        companyService.deleteCompany(id);
        LOGGER.info("Company with ID: {} was deleted.", id);
    }

    @GetMapping("/ares/{companyId}")
    @ResponseStatus(HttpStatus.OK)
    public CompanyDto findBy(@PathVariable String companyId) throws DatatypeConfigurationException {
        LOGGER.info("Endpoint find company with company ID: {} on Ares webservice was called.", companyId);
        CompanyDto companyDto = companyAresService.findByCompanyId(companyId);
        LOGGER.info("Company {} was returned and created.", companyDto);
        return companyDto;
    }
}