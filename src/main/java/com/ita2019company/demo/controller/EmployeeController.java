package com.ita2019company.demo.controller;

import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<EmployeeDto> findAll(long companyId) {
        LOGGER.info("Endpoint find all employees was called.");
        List<EmployeeDto> employeeDtoList = employeeService.findAll(companyId);
        LOGGER.info("Returned was {} employees.", employeeDtoList.size());
        return employeeDtoList;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public EmployeeDto updateEmployee(@RequestBody EmployeeDto employee) {
        LOGGER.info("Endpoint update employee was called on employee with ID: {}.", employee.getId());
        EmployeeDto newEmployee = employeeService.update(employee);
        LOGGER.info("Employee with ID: {} was updated to {}.", employee.getId(), newEmployee);
        return newEmployee;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeDto createEmployee(@RequestBody EmployeeDto employee) {
        LOGGER.info("Endpoint create employee was called.");
        EmployeeDto employeeDto = employeeService.create(employee);
        LOGGER.info("Employee {} was created.", employeeDto);
        return employeeDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete employee with ID: {} was called.", id);
        employeeService.delete(id);
        LOGGER.info("Employee with ID: {} was deleted.", id);
    }
}