package com.ita2019company.demo.controller;

import com.ita2019company.demo.dto.ProjectDto;
import com.ita2019company.demo.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProjectDto> findAll() {
        LOGGER.info("Endpoint find all projects was called.");
        List<ProjectDto> projectDtoList = projectService.findAll();
        LOGGER.info("Returned was {} projects.", projectDtoList.size());
        return projectDtoList;
    }

    @GetMapping(params = {"name", "createdAfter"})
    @ResponseStatus(HttpStatus.OK)
    public List<ProjectDto> findAll(@RequestParam("name") String name, @RequestParam("createdAfter") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            LocalDateTime createdAfter) {
        LOGGER.info("Endpoint find all projects with name: {} and created after: {} was called.", name, createdAfter);
        List<ProjectDto> projectDtoList = projectService.findAll(name, createdAfter);
        LOGGER.info("Returned was {} projects.", projectDtoList.size());
        return projectDtoList;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ProjectDto updateProject(@RequestBody ProjectDto project) {
        LOGGER.info("Endpoint update project was called on project with ID: {}.", project.getId());
        ProjectDto newProject = projectService.update(project);
        LOGGER.info("Project with ID: {} was updated to {}.", project.getId(), newProject);
        return newProject;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProjectDto createProject(@RequestBody ProjectDto project) {
        LOGGER.info("Endpoint create project was called.");
        ProjectDto projectDto = projectService.create(project);
        LOGGER.info("Project {} was created.", projectDto);
        return projectDto;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@RequestParam("id") long id) {
        LOGGER.info("Endpoint delete project with ID: {} was called.", id);
        projectService.delete(id);
        LOGGER.info("Project with ID: {} was deleted.", id);
    }
}