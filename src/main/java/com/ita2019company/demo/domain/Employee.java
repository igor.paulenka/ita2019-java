package com.ita2019company.demo.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "EMPLOYEE")
@NamedQuery(name = Employee.QUERY_ALL, query = "SELECT e FROM Employee e JOIN FETCH e.companyBranch cobr JOIN FETCH cobr.company c WHERE c.id = ?1")
public class Employee extends AbstractEntity {

    public static final String QUERY_ALL = "query.Employee.all";

    @Column(name = "FIRST_NAME", length = 20, nullable = false)
    private String firstName;
    @Column(name = "SURNAME", length = 40, nullable = false)
    private String surname;
    @Column(name = "SALARY", nullable = false)
    private long salary;
    @ManyToOne(optional = false)
    @JoinColumn(name = "COMPANY_BRANCH_ID", foreignKey = @ForeignKey(name = "EMPLOYEE_COMPANY_BRANCH_FK"))
    private CompanyBranch companyBranch;
    @ManyToMany(mappedBy = "employees")
    private List<Project> projects;

    public String getFirstName() {
        return firstName;
    }

    public Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Employee setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public long getSalary() {
        return salary;
    }

    public Employee setSalary(long salary) {
        this.salary = salary;
        return this;
    }

    public CompanyBranch getCompanyBranch() {
        return companyBranch;
    }

    public Employee setCompanyBranch(CompanyBranch companyBranch) {
        this.companyBranch = companyBranch;
        return this;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Employee setProjects(List<Project> projects) {
        this.projects = projects;
        return this;
    }
}