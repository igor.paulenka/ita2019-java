package com.ita2019company.demo.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "COMPANY")
public class Company extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_GEN")
    @SequenceGenerator(name = "ID_GEN", schema = "ita2019", sequenceName = "company_id_sequence", allocationSize = 1)
    @Column(name = "ID")
    private long id;
    @Column(name = "COMPANY_NAME", length = 100, nullable = false, unique = true)
    private String name;
    @Column(name = "COMPANY_ID", length = 8, nullable = false, unique = true)
    private String companyId;
    @Column(name = "VAT_ID", length = 10)
    private String vatId;
    @Column(name = "WEB_URL", length = 100)
    private String webUrl;
    @Column(name = "NOTE", length = 500)
    private String note;
    @Column(name = "ACTIVE")
    private boolean active;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(
                    name = "city",
                    column = @Column(name = "ADDRESS_CITY", length = 30, nullable = false)
            ),
            @AttributeOverride(
                    name = "street",
                    column = @Column(name = "ADDRESS_STREET", length = 30, nullable = false)
            ),
            @AttributeOverride(
                    name = "houseNumber",
                    column = @Column(name = "ADDRESS_HOUSE_NUMBER", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "zipCode",
                    column = @Column(name = "ADDRESS_ZIP_CODE", length = 12, nullable = false)
            ),
            @AttributeOverride(
                    name = "country",
                    column = @Column(name = "ADDRESS_COUNTRY", length = 3, nullable = false)
            )
    })
    private Address headquarters;
    @OneToMany(mappedBy = "company")
    private List<CompanyBranch> companyBranches;
    @OneToMany(mappedBy = "company")
    private List<Project> projects;

    public Company(long id, String name, String companyId, String vatId, String webUrl, String note, boolean active, Address headquarters, List<CompanyBranch> companyBranches, List<Project> projects) {
        this.id = id;
        this.name = name;
        this.companyId = companyId;
        this.vatId = vatId;
        this.webUrl = webUrl;
        this.note = note;
        this.active = active;
        this.headquarters = headquarters;
        this.companyBranches = companyBranches;
        this.projects = projects;
    }

    public Company() {
    }

    public long getId() {
        return id;
    }

    public Company setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Company setName(String name) {
        this.name = name;
        return this;
    }

    public String getCompanyId() {
        return companyId;
    }

    public Company setCompanyId(String companyId) {
        this.companyId = companyId;
        return this;
    }

    public String getVatId() {
        return vatId;
    }

    public Company setVatId(String vatId) {
        this.vatId = vatId;
        return this;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public Company setWebUrl(String webUrl) {
        this.webUrl = webUrl;
        return this;
    }

    public String getNote() {
        return note;
    }

    public Company setNote(String note) {
        this.note = note;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public Company setActive(boolean active) {
        this.active = active;
        return this;
    }

    public Address getHeadquarters() {
        return headquarters;
    }

    public Company setHeadquarters(Address headquarters) {
        this.headquarters = headquarters;
        return this;
    }

    public List<CompanyBranch> getCompanyBranches() {
        return companyBranches;
    }

    public Company setCompanyBranches(List<CompanyBranch> companyBranches) {
        this.companyBranches = companyBranches;
        return this;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Company setProjects(List<Project> projects) {
        this.projects = projects;
        return this;
    }
}