package com.ita2019company.demo.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PROJECT")
@NamedQueries({
        @NamedQuery(name = Project.QUERY_ALL, query = "SELECT p FROM Project p"),
        @NamedQuery(name = Project.QUERY_ALL_NAME_CREATION_TIME, query = "SELECT p FROM Project p WHERE p.name LIKE ?1 AND p.createdAt > ?2")
})
public class Project extends AbstractEntity {

    public static final String QUERY_ALL = "query.Project.all";
    public static final String QUERY_ALL_NAME_CREATION_TIME = "query.Project.allByNameAndCreationTime";

    @Column(name = "NAME", nullable = false, length = 150)
    private String name;
    @Column(name = "START_DATE", length = 10)
    private String startDate;
    @Column(name = "END_DATE", length = 10)
    private String endDate;
    @Column(name = "PRICE")
    private long price;
    @ManyToOne(optional = false)
    @JoinColumn(name = "COMPANY_ID", foreignKey = @ForeignKey(name = "PROJECT_COMPANY_FK"))
    private Company company;
    @ManyToMany
    @JoinTable(name = "PROJECT_EMPLOYEE", joinColumns = @JoinColumn(name = "PROJECT_ID",
            foreignKey = @ForeignKey(name = "PROJECT_EMPLOYEE_PROJECT_FK")
    ),
            inverseJoinColumns = @JoinColumn(name = "EMPLOYEE_ID",
                    foreignKey = @ForeignKey(name = "PROJECT_EMPLOYEE_EMPLOYEE_FK")
            )
    )
    private List<Employee> employees;

    public String getName() {
        return name;
    }

    public Project setName(String name) {
        this.name = name;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public Project setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public Project setEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public long getPrice() {
        return price;
    }

    public Project setPrice(long price) {
        this.price = price;
        return this;
    }

    public Company getCompany() {
        return company;
    }

    public Project setCompany(Company company) {
        this.company = company;
        return this;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Project setEmployees(List<Employee> employees) {
        this.employees = employees;
        return this;
    }
}