package com.ita2019company.demo.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "COMPANY_BRANCH")
@NamedQuery(name = CompanyBranch.QUERY_ALL, query = "SELECT cb FROM CompanyBranch cb JOIN FETCH cb.company c WHERE c.id = ?1")
public class CompanyBranch extends AbstractEntity {

    public static final String QUERY_ALL = "query.CompanyBranch.all";

    @Column(name = "NAME", nullable = false, length = 150)
    private String name;
    @AttributeOverrides({
            @AttributeOverride(
                    name = "city",
                    column = @Column(name = "ADDRESS_CITY", length = 30, nullable = false)
            ),
            @AttributeOverride(
                    name = "street",
                    column = @Column(name = "ADDRESS_STREET", length = 30, nullable = false)
            ),
            @AttributeOverride(
                    name = "houseNumber",
                    column = @Column(name = "ADDRESS_HOUSE_NUMBER", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "zipCode",
                    column = @Column(name = "ADDRESS_ZIP_CODE", length = 12, nullable = false)
            ),
            @AttributeOverride(
                    name = "country",
                    column = @Column(name = "ADDRESS_COUNTRY", length = 3, nullable = false)
            )
    })
    private Address location;
    @OneToMany(mappedBy = "companyBranch")
    private List<Employee> employees;
    @ManyToOne(optional = false)
    @JoinColumn(name = "COMPANY_ID", foreignKey = @ForeignKey(name = "COMPANY_BRANCH_COMPANY_FK"))
    private Company company;

    public String getName() {
        return name;
    }

    public CompanyBranch setName(String name) {
        this.name = name;
        return this;
    }

    public Address getLocation() {
        return location;
    }

    public CompanyBranch setLocation(Address location) {
        this.location = location;
        return this;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public CompanyBranch setEmployees(List<Employee> employees) {
        this.employees = employees;
        return this;
    }

    public Company getCompany() {
        return company;
    }

    public CompanyBranch setCompany(Company company) {
        this.company = company;
        return this;
    }
}