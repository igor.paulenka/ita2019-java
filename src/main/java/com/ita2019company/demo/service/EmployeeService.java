package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.EmployeeDto;

import java.util.List;

/**
 * Service interface with declaration of methods which serves the Employee class.
 */
public interface EmployeeService {

    /**
     * Method find all employees that meet inserted condition.
     *
     * @param companyId Identification parameter (primary key) for company. Must not be null or negative.
     * @return List of all employees that satisfy the specified requirement with related company branch and the company they belong to.
     */
    List<EmployeeDto> findAll(long companyId);

    /**
     * Method creates new employee.
     *
     * @param employee The employee to be created.
     * @return Created employee.
     */
    EmployeeDto create(EmployeeDto employee);

    /**
     * Method updates existing employee.
     *
     * @param employee The employee to be updated.
     * @return Updated employee.
     */
    EmployeeDto update(EmployeeDto employee);

    /**
     * Method deletes employee chosen by employees id.
     *
     * @param id Identification parameter (primary key) for employee. Must not be null or negative.
     */
    void delete(long id);
}