package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.Company;
import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.dto.mapping.CompanyMapper;
import com.ita2019company.demo.exception.RecordNotFoundException;
import com.ita2019company.demo.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper;

    public CompanyServiceImpl(CompanyRepository companyRepository, CompanyMapper companyMapper) {
        this.companyRepository = companyRepository;
        this.companyMapper = companyMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CompanyDto> findAll() {
        LOGGER.info("Find all companies method was called.");
        List<CompanyDto> foundedCompany = companyRepository.findAll()
                .stream()
                .map(companyMapper::companyToCompanyDto)
                .collect(Collectors.toList());
        foundedCompany.sort(Comparator.comparing(CompanyDto::getName));
        LOGGER.info("Number of returned companies: {}.", foundedCompany.size());
        return foundedCompany;
    }

    @Override
    public CompanyDto updateCompany(CompanyDto company) {
        LOGGER.info("Update company method was called on company: {}.", company);
        try {
            Company entity = companyMapper.companyDtoToCompany(company);
            companyRepository.getOne(entity.getId());
            Company newEntity = companyRepository.save(entity);
            CompanyDto dto = companyMapper.companyToCompanyDto(newEntity);
            LOGGER.info("Company was updated to {}.", newEntity);
            return dto;
        } catch (JpaObjectRetrievalFailureException e) {
            throw new RecordNotFoundException(String.format("Company with Id '%s' not found", company.getId()));
        }
    }

    @Override
    public CompanyDto createCompany(CompanyDto company) {
        LOGGER.info("Create company method was called.");
        try {
            Company createdCompany = companyMapper.companyDtoToCompany(company);
            companyRepository.save(createdCompany);
            LOGGER.info("Company {} was created.", createdCompany);
            return companyMapper.companyToCompanyDto(createdCompany);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException("Duplicate values was inserted.");
        }
    }

    @Override
    public void deleteCompany(long id) {
        LOGGER.info("Delete company method was called on company with ID: {}.", id);
        try {
            companyRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new RecordNotFoundException(String.format("Company with Id '%s' not found", id));
        }
        LOGGER.info("Company with ID {} was deleted.", id);
    }
}