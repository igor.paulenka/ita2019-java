package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.Employee;
import com.ita2019company.demo.dto.EmployeeDto;
import com.ita2019company.demo.dto.mapping.EmployeeMapper;
import com.ita2019company.demo.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findAll(long employeeId) {
        LOGGER.info("Find all employees method was called.");
        List<EmployeeDto> employeeDtoList = employeeRepository.findAll(employeeId)
                .stream()
                .map(employeeMapper::employeeToEmployeeDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned employees: {}.", employeeDtoList.size());
        return employeeDtoList;
    }

    @Override
    public EmployeeDto create(EmployeeDto employee) {
        LOGGER.info("Create employee method was called.");
        Employee entity = employeeMapper.employeeDtoToEmployee(employee);
        Employee newEntity = employeeRepository.create(entity);
        EmployeeDto dto = employeeMapper.employeeToEmployeeDto(newEntity);
        LOGGER.info("Employee {} was created.", newEntity);
        return dto;
    }

    @Override
    public EmployeeDto update(EmployeeDto employee) {
        Employee entity = employeeMapper.employeeDtoToEmployee(employee);
        LOGGER.info("Update employee method was called on employee: {}.", entity);

        Employee newEntity = employeeRepository.update(entity);
        EmployeeDto dto = employeeMapper.employeeToEmployeeDto(newEntity);
        LOGGER.info("Employee was updated to {}.", newEntity);
        return dto;
    }

    @Override
    public void delete(long id) {
        Objects.requireNonNull(id);
        LOGGER.info("Delete employee method was called on employee with ID: {}.", id);
        employeeRepository.delete(id);
        LOGGER.info("Employee with ID {} was deleted.", id);
    }
}