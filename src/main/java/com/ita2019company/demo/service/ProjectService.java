package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.ProjectDto;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Service interface with declaration of methods which serves the Project class.
 */
public interface ProjectService {

    /**
     * Method find all projects.
     *
     * @return List of all projects, employees working on them and the company they belong to.
     */
    List<ProjectDto> findAll();

    /**
     * Method find all projects that meet inserted conditions.
     *
     * @param name         Only project with name that contained this substring will be selected.
     * @param createdAfter Only project that was create after this inserted date and time will be selected.
     * @return List of projects that satisfy all the specified requirement with employees working on them and the company they belong to.
     */
    List<ProjectDto> findAll(String name, LocalDateTime createdAfter);

    /**
     * Method creates new project.
     *
     * @param project The project to be created.
     * @return Created project.
     */
    ProjectDto create(ProjectDto project);

    /**
     * Method updates existing project.
     *
     * @param project The project to be updated.
     * @return Updated project.
     */
    ProjectDto update(ProjectDto project);

    /**
     * Method deletes project chosen by projects id.
     *
     * @param id Identification parameter (primary key) for projects. Must not be null or negative.
     */
    void delete(long id);
}