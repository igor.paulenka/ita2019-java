package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.CompanyBranchDto;

import java.util.List;

/**
 * Service interface with declaration of methods which serves the CompanyBranch class.
 */
public interface CompanyBranchService {

    /**
     * Method find all company branches that meet inserted condition.
     *
     * @param companyId Identification parameter (primary key) for company. Must not be null or negative.
     * @return List of all company branches of the company.
     */
    List<CompanyBranchDto> findAll(long companyId);

    /**
     * Method creates new company branch.
     *
     * @param companyBranch The company branch to be created.
     * @return Created company branch.
     */
    CompanyBranchDto create(CompanyBranchDto companyBranch);

    /**
     * Method updates existing company branch.
     *
     * @param companyBranch The company branch to be updated.
     * @return Updated company branch.
     */
    CompanyBranchDto update(CompanyBranchDto companyBranch);

    /**
     * Method deletes companyBranch chosen by id of company branch .
     *
     * @param id Identification parameter (primary key) for company branch. Must not be null or negative.
     */
    void delete(long id);
}