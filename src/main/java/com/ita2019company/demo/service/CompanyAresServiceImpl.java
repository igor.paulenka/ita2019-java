package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.webservices.ares.AresRequestBuilder;
import com.ita2019company.demo.webservices.ares.CompanyAresMapper;
import cz.mfcr.ares.answer.AresOdpovedi;
import cz.mfcr.ares.request.AresDotazy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import javax.xml.datatype.DatatypeConfigurationException;

@Service
public class CompanyAresServiceImpl extends WebServiceGatewaySupport implements CompanyAresService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyAresServiceImpl.class);

    private final WebServiceTemplate webServiceTemplate;
    private final AresRequestBuilder aresRequestBuilder;
    private final CompanyAresMapper companyAresMapper;
    private final CompanyService companyService;

    public CompanyAresServiceImpl(WebServiceTemplate webServiceTemplate, AresRequestBuilder aresRequestBuilder, CompanyAresMapper companyAresMapper, CompanyService companyService) {
        this.webServiceTemplate = webServiceTemplate;
        this.aresRequestBuilder = aresRequestBuilder;
        this.companyAresMapper = companyAresMapper;
        this.companyService = companyService;
    }

    public CompanyDto findByCompanyId(String companyId) throws DatatypeConfigurationException {
        LOGGER.info("Method find and create company by companyId: {} on ARES webservice was called.", companyId);
        AresDotazy request = aresRequestBuilder.createRequest(companyId);
        AresOdpovedi response = (AresOdpovedi) webServiceTemplate.marshalSendAndReceive(request);
        CompanyDto companyDto = mapAndCreate(response);
        return companyDto;
    }

    private CompanyDto mapAndCreate(AresOdpovedi response) {
        CompanyDto foundedDto = companyAresMapper.aresOdpovedToCompany(response);
        CompanyDto createdDto = companyService.createCompany(foundedDto);
        LOGGER.info("Company {} was retrieved from server and created.", createdDto);
        return createdDto;
    }
}