package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.Project;
import com.ita2019company.demo.dto.ProjectDto;
import com.ita2019company.demo.dto.mapping.ProjectMapper;
import com.ita2019company.demo.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);

    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    public ProjectServiceImpl(ProjectRepository projectRepository, ProjectMapper projectMapper) {
        this.projectRepository = projectRepository;
        this.projectMapper = projectMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProjectDto> findAll() {
        LOGGER.info("Find all projects method was called.");
        List<ProjectDto> projectDtoList = projectRepository.findAll()
                .stream()
                .map(projectMapper::projectToProjectDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned projects: {}.", projectDtoList.size());
        return projectDtoList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProjectDto> findAll(String name, LocalDateTime createdAfter) {
        LOGGER.info("Method find all projects with name: {} and created after: {} was called.", name, createdAfter);
        List<ProjectDto> projectDtoList = projectRepository.findAll(name, createdAfter)
                .stream()
                .map(projectMapper::projectToProjectDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of projects employees: {}.", projectDtoList.size());
        return projectDtoList;
    }

    @Override
    public ProjectDto create(ProjectDto project) {
        LOGGER.info("Create project method was called.");
        Project entity = projectMapper.projectDtoToProject(project);
        Project newEntity = projectRepository.create(entity);
        ProjectDto dto = projectMapper.projectToProjectDto(newEntity);
        LOGGER.info("Project {} was created.", newEntity);
        return dto;
    }

    @Override
    public ProjectDto update(ProjectDto project) {
        Project entity = projectMapper.projectDtoToProject(project);
        LOGGER.info("Update project method was called on project: {}.", entity);
        Project newEntity = projectRepository.update(entity);
        ProjectDto dto = projectMapper.projectToProjectDto(newEntity);
        LOGGER.info("Project was updated to {}.", newEntity);
        return dto;
    }

    @Override
    public void delete(long id) {
        Objects.requireNonNull(id);
        LOGGER.info("Delete project method was called on project with ID: {}.", id);
        projectRepository.delete(id);
        LOGGER.info("Project with ID {} was deleted.", id);
    }
}