package com.ita2019company.demo.service;

import com.ita2019company.demo.domain.CompanyBranch;
import com.ita2019company.demo.dto.CompanyBranchDto;
import com.ita2019company.demo.dto.mapping.CompanyBranchMapper;
import com.ita2019company.demo.repository.CompanyBranchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompanyBranchServiceImpl implements CompanyBranchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyBranchServiceImpl.class);

    private final CompanyBranchRepository companyBranchRepository;
    private final CompanyBranchMapper companyBranchMapper;

    public CompanyBranchServiceImpl(CompanyBranchRepository companyBranchRepository, CompanyBranchMapper companyBranchMapper) {
        this.companyBranchRepository = companyBranchRepository;
        this.companyBranchMapper = companyBranchMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CompanyBranchDto> findAll(long companyBranchId) {
        LOGGER.info("Method find all company branches was called.");
        List<CompanyBranchDto> companyBranchDtoList = companyBranchRepository.findAll(companyBranchId)
                .stream()
                .map(companyBranchMapper::companyBranchToCompanyBranchDto)
                .collect(Collectors.toList());
        LOGGER.info("Number of returned company branches: {}.", companyBranchDtoList.size());
        return companyBranchDtoList;
    }

    @Override
    public CompanyBranchDto create(CompanyBranchDto companyBranch) {
        LOGGER.info("Method create company branch was called.");
        CompanyBranch entity = companyBranchMapper.companyBranchDtoToCompanyBranch(companyBranch);
        CompanyBranch newEntity = companyBranchRepository.create(entity);
        CompanyBranchDto dto = companyBranchMapper.companyBranchToCompanyBranchDto(newEntity);
        LOGGER.info("Company branch {} was created.", newEntity);
        return dto;
    }

    @Override
    public CompanyBranchDto update(CompanyBranchDto companyBranch) {
        CompanyBranch entity = companyBranchMapper.companyBranchDtoToCompanyBranch(companyBranch);
        LOGGER.info("Method update company branch was called on company branch: {}.", entity);
        CompanyBranch newEntity = companyBranchRepository.update(entity);
        CompanyBranchDto dto = companyBranchMapper.companyBranchToCompanyBranchDto(newEntity);
        LOGGER.info("Company branch was updated to {}.", newEntity);
        return dto;
    }

    @Override
    public void delete(long id) {
        Objects.requireNonNull(id);
        LOGGER.info("Method delete company branch was called on company branch with ID: {}.", id);
        companyBranchRepository.delete(id);
        LOGGER.info("Company branch with ID {} was deleted.", id);
    }
}