package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.CompanyDto;
import com.ita2019company.demo.exception.RecordNotFoundException;

import java.util.List;


/**
 * Service interface with declaration of methods which serves the Company class.
 */
public interface CompanyService {

    /**
     * Method find all companies which were created and saved.
     *
     * @return List of all companies sorted alphabetically by company name.
     */
    List<CompanyDto> findAll();

    /**
     * Method updates company attributes.
     *
     * @return Updated company.
     * @throws IllegalArgumentException with message if company with inserted id doesn't exist.
     */
    CompanyDto updateCompany(CompanyDto company);

    /**
     * Method creates new company.
     *
     * @return Created company
     */
    CompanyDto createCompany(CompanyDto company);

    /**
     * Method deletes company chosen by company id.
     *
     * @param id identification parameter for companies. Must not be null or negative
     * @throws RecordNotFoundException with message if company with inserted id doesn't exist.
     */
    void deleteCompany(long id);
}