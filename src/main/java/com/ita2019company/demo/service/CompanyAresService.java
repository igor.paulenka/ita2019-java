package com.ita2019company.demo.service;

import com.ita2019company.demo.dto.CompanyDto;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * Service interface with declaration of method which communicates with Ares webservice and convert response into new company.
 */
public interface CompanyAresService {

    /**
     * Method find, return and create company by ARES webservice.
     *
     * @param companyId Identification parameter for companies (ICO). Must not be negative and must contain sequence of 8 digits.
     * @return CompanyDto with parameters retrieved from ARES.
     * @throws DatatypeConfigurationException
     */
    CompanyDto findByCompanyId(String companyId) throws DatatypeConfigurationException;
}