INSERT INTO COMPANY_BRANCH (NAME, CREATED_AT, LAST_UPDATE, ADDRESS_CITY, ADDRESS_STREET, ADDRESS_HOUSE_NUMBER,
                            ADDRESS_ZIP_CODE, ADDRESS_COUNTRY, COMPANY_ID)
VALUES ('Hypermarket Brno - Modřice', now(), now(), 'Brno - Modřice', 'U dálnice', '744', '66442 ', 'CZE', '1'),
       ('Supermarket Brno Rozkvět', now(), now(), 'Brno', 'náměstí Svobody', '85/16', '60200', 'CZE', '1'),
       ('Billa Liptovský Mikuláš', now(), now(), 'Liptovský Mikuláš', 'Garbiarska', '2585', '03101', 'SVK', '2');
