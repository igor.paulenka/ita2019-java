INSERT INTO COMPANY (COMPANY_NAME, COMPANY_ID, VAT_ID, WEB_URL, NOTE, ACTIVE, CREATED_AT, LAST_UPDATE, ADDRESS_CITY,
                     ADDRESS_STREET, ADDRESS_HOUSE_NUMBER, ADDRESS_ZIP_CODE, ADDRESS_COUNTRY)
VALUES ('Albert Česká republika, s.r.o.', '44012373', 'CZ44012373', 'www.albert.cz', 'Albert company note', true, now(),
        now(), 'Praha - Jinonice', 'Radlická', '520/117', '15800', 'CZE'),
       ('BILLA, spol. s.r.o.', '00685976', 'CZ00685976', 'www.billa.cz', 'Billa company note', true, now(), now(),
        'Říčany u Prahy', 'Modletice', '67', '25101', 'CZE');