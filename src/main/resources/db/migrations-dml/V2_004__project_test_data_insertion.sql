INSERT INTO PROJECT (NAME, START_DATE, END_DATE, PRICE, CREATED_AT, LAST_UPDATE, COMPANY_ID)
VALUES ('Table of sales creation', '20.1.2020', '1.2.2020', 25000, now(), now(), 1),
       ('Rebuilding entrance area', '1.3.2020', '5.6.2020', 620000, now(), now(), 2),
       ('Locker room upgrade', '25.2.2020', '29.2.2020', 190000, now(), now(), 2);