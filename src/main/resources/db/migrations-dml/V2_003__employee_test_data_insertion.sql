INSERT INTO EMPLOYEE (FIRST_NAME, SURNAME, SALARY, CREATED_AT, LAST_UPDATE, COMPANY_BRANCH_ID)
VALUES ('Aleš', 'Albert', 25000, now(), now(), 1),
       ('Barbora', 'Bortová', 28000, now(), now(), 1),
       ('Ciril', 'Černák', 31000, now(), now(), 2),
       ('Denis', 'Debnár', 29000, now(), now(), 2),
       ('Emil', 'Etinolák', 21000, now(), now(), 3),
       ('František', 'Ferdinand', 45000, now(), now(), 3);