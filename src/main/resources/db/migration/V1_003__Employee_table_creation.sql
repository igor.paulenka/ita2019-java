CREATE TABLE EMPLOYEE
(
    ID                BIGINT PRIMARY KEY NOT NULL,
    FIRST_NAME        VARCHAR(20)        NOT NULL,
    SURNAME           VARCHAR(40)        NOT NULL,
    SALARY            BIGINT             NOT NULL,
    CREATED_AT        TIMESTAMP WITHOUT TIME ZONE,
    LAST_UPDATE       TIMESTAMP WITHOUT TIME ZONE,
    COMPANY_BRANCH_ID BIGINT
);

CREATE SEQUENCE IF NOT EXISTS employee_id_sequence OWNED BY employee.id;
ALTER TABLE EMPLOYEE
    ALTER COLUMN ID SET DEFAULT nextval('employee_id_sequence');
ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_company_branch_fk FOREIGN KEY (company_branch_id) REFERENCES company_branch (id);