CREATE TABLE PROJECT
(
    ID          BIGINT PRIMARY KEY NOT NULL,
    NAME        VARCHAR(150)       NOT NULL,
    START_DATE  VARCHAR(10),
    END_DATE    VARCHAR(10),
    PRICE       BIGINT,
    CREATED_AT  TIMESTAMP WITHOUT TIME ZONE,
    LAST_UPDATE TIMESTAMP WITHOUT TIME ZONE,
    COMPANY_ID  BIGINT
);

CREATE SEQUENCE IF NOT EXISTS project_id_sequence OWNED BY project.id;
ALTER TABLE PROJECT
    ALTER COLUMN ID SET DEFAULT nextval('project_id_sequence');
ALTER TABLE ONLY project
    ADD CONSTRAINT project_company_fk FOREIGN KEY (company_id) REFERENCES company (id);