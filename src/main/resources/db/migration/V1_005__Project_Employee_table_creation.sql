CREATE TABLE project_employee
(
    PROJECT_ID  BIGINT NOT NULL,
    EMPLOYEE_ID BIGINT NOT NULL
);

ALTER TABLE ONLY project_employee
    ADD CONSTRAINT project_employee_employee_fk FOREIGN KEY (EMPLOYEE_ID) REFERENCES employee (id);
ALTER TABLE ONLY project_employee
    ADD CONSTRAINT project_employee_project_fk FOREIGN KEY (PROJECT_ID) REFERENCES project (id);