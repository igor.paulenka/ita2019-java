# Application ITAJ-2019
This application was created as an educational project focused on Java Spring and serve as skills presentation.
Application ITAJ-2019 is currently deployed and running on [Heroku Cloud](https://heroku-itaj-2019-env-prod.herokuapp.com/).

### Table of content
1.  [Built With](#built-with)
    *   [External Tools Used](#external-tools-used)
2.  [Actuator](#actuator)
3.  [Features](#features)
4.  [Endpoints](#endpoints)
    *   [Status codes](#status-codes)
5.  [Installing](#installing)
6.  [Deploying](#deploying)

## Built With
* 	[Maven](https://maven.apache.org/) - Dependency Management
* 	[Flyway](https://flywaydb.org/) - Version control for database
* 	[JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java™ Platform, Standard Edition Development Kit 
* 	[Spring Boot](https://spring.io/projects/spring-boot) - Framework to ease the bootstrapping and development of new Spring Applications
* 	[PostgreSQL](https://www.postgresql.org/) - Open-Source Object-Relational Database System
* 	[git](https://git-scm.com/) - Free and Open-Source distributed version control system 
### External Tools Used
* [Postman](https://www.getpostman.com/) - API Development Environment

## Actuator
To monitor and manage application are available this actuators:

|  URL                                                                  |Method |
|-----------------------------------------------------------------------|:-----:|
|`https://heroku-itaj-2019-env-prod.herokuapp.com/actuator`             | `GET` |
|`https://heroku-itaj-2019-env-prod.herokuapp.com/actuator/health`    	| `GET` |
|`https://heroku-itaj-2019-env-prod.herokuapp.com/actuator/info`      	| `GET` |
|`https://heroku-itaj-2019-env-prod.herokuapp.com/actuator/env`         | `GET` |
|`https://heroku-itaj-2019-env-prod.herokuapp.com/actuator/heapdump`    | `GET` |

*Credentials for access are:*
*   login: **appAdmin**
*   password: **appPassword**

## Features
Application define CRUD operations for four entities.
*   Company
*   Company Branch
*   Employee
*   Project

This entities are arrange in cycle dependencies with relationship model define as:

`COMPANY` 1 <=> n `COMPANY BRANCH` 1 <=> n `EMPLOYEE` n <=> n `PROJECT` n <=> 1 `COMPANY`

Application also integrate ARES (`Administrative register of economic subjects`) webservice features. 
That allows search and save company entities from database provided by [MFCR](http://wwwinfo.mfcr.cz/ares/ares.html).

## Endpoints
For communication with endpoints must be `Content-type` as `application/json` specified.
Assessable endpoints are:

|       |Endpoint                                                                 |Request type                    |
|:-----:|-------------------------------------------------------------------------|--------------------------------|
|1      | [/company/](heroku-itaj-2019-env-prod.herokuapp.com/company)            |`GET`, `PUT`, `POST` or `DELETE`|
|2      | [/companybranch/](heroku-itaj-2019-env-prod.herokuapp.com/companybranch)|`GET`, `PUT`, `POST` or `DELETE`|
|3      | [/employee/](heroku-itaj-2019-env-prod.herokuapp.com/employee)          |`GET`, `PUT`, `POST` or `DELETE`|
|4      | [/project/](heroku-itaj-2019-env-prod.herokuapp.com/project)            |`GET`, `PUT`, `POST` or `DELETE`|
|5      | [/company/ares/](heroku-itaj-2019-env-prod.herokuapp.com/company/ares/) |`GET`                           |

Basic information for communication with REST API for entities are listed in following tables.
*   Entity: **Company**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/company`| |List of all companies|
|`POST`|`/company`| |Created company|
|`PUT`|`/company`| |Updated company|
|`DELETE`|`/company`|`id`| |
|`GET`|`/company/ares/`|`*`|Created company|

`id` - primary key of company

`*` - `GET` request for ARES webservice required pathVariable in form:

`company/ares/{companyId}`. Variable companyId must be real and contain sequence of 8 digits.

*   Entity: **Company Branch**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/companybranch`|`companyId`|Company with their branches|
|`POST`|`/companybranch`| |Created company branch|
|`PUT`|`/companybranch`| |Updated company branch|
|`DELETE`|`/companybranch`|`id`| |

`companyId` - primary key (`id`) of company entity which own searching company branches

`id` - primary key of company branch entity

*   Entity: **Employee**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/employee`|`companyId`|Company with list of their branches with list of their employees|
|`POST`|`/employee`| |Created employee|
|`PUT`|`/employee`| |Updated employee|
|`DELETE`|`/employee`|`id`| |

`companyId` - primary key of company entity (`id`) which own searching employees

`id` - primary key of employee entity

*   Entity: **Project**

|Request type| Endpoint | Request Parameter|Returned|
|:---:|---|:---:|---|
|`GET`|`/project`|`companyId`|List of all projects|
|`GET`|`/project`|`name`, `createdAfter`|List of projects|
|`POST`|`/project`| |Created project|
|`PUT`|`/project`| |Updated project|
|`DELETE`|`/project`|`id`| |

`companyId` - primary key of company entity (id) which own searching employees

`name` - substring of existing project name. Projects that contains this substring will be returned.

`createdAfter` - time in ISO DateTime Format: **yyyy-MM-dd'T'HH:mm:ss.SSSXXX**, returned will be only projects that were created after this date and time.

`id` - primary key of employee entity

More information and templates for basic requests are available in [REST_API_TEMPLATE](REST_API_TEMPLATE.md) file.

### Status codes

The API is designed to return different status codes according to context and action. 
This way, if a request results in an error, the caller is able to get insight into what went wrong.

The following table gives an overview of how the API functions behave.

| Request type | Description |
| ------------ | ----------- |
| `GET`   | Access one or more resources and return the result as JSON. |
| `POST`  | Return `201 Created` if the resource is successfully created and return the newly created resource as JSON. |
| `GET` / `PUT` | Return `200 OK` if the resource is accessed or modified successfully. The (modified) result is returned as JSON. |
| `DELETE` | Returns `204 No Content` if the resource was deleted successfully. |

The following table shows the possible return codes for API requests.

| Return values | Description |
| ------------- | ----------- |
| `200 OK` | The `GET`, `PUT` request was successful, the resource(s) itself is returned as JSON. |
| `204 No Content` | The server has successfully fulfilled the  `DELETE` request and that there is no additional content to send in the response payload body. |
| `201 Created` | The `POST` request was successful and the resource is returned as JSON. |
| `400 Bad Request` | A required attribute of the API request is missing. |
| `404 Not Found` | A resource could not be accessed, e.g., an ID for a resource could not be found. |
| `500 Server Error` | While handling the request something went wrong server-side. |

## Installing
Application is available on [gitlab](https://gitlab.com/igor.paulenka/ita2019-java). 
Public access for ITAJ-2019 allows run app locally by clone git repository:
  
  `git clone https://github.com/spring-projects/spring-petclinic.git`

For run can be used by external tools (Intellij IDEA, Eclipse,..) or execution:
 
  ```shell
  mvn spring-boot:run
```
Application run by default with active profile `dev`. 
Properties for this profile is located inside files:

 `src/main/resources/application.properties`
 
 `src/main/resources/application-localhost.properties`
 
Profile `it` served for integration tests of repository layer during verify phase. 
For tests is used external database on [elephantsql.com](https://www.elephantsql.com/).
Profile properties is located in file:

`src/test/resources/application-it.properties`

## Deploying
Application is automatic deploy on heroku cloud by committing into the master branch through gitlab CI. 
Script for this deploying is located in `.gitlab-ci.yml` file. 
For deployment active profile `prod` is used. 
Properties for this profile is located inside files:

 ``src/main/resources/application.properties``
 
 ``src/main/resources/application-heroku.properties`` 

Profile `it` served for integration tests of repository layer during verify phase. 
For tests is used external database on [elephantsql.com](https://www.elephantsql.com/). 
Profile properties is located in file:

``src/test/resources/application-it.properties``