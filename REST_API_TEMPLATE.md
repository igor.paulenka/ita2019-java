# REST API

This file contain basics templates for communication with REST API. This requests could be modified. 
Request and response variables can be changed by modifying `MapStruct` settings in `mapping` package.

If application run locally endpoins are accessable by default on:

`https://localhost:8080/{ENDPOINTS}`

If run on heroku cloud url for endpoints is in form:

`https://heroku-itaj-2019-env-prod.herokuapp.com/{ENDPOINTS}`

Possible ENDPOINTS are: 
1.  [company](#company)
2.  [companybranch](#companybranch)
3.  [project](#project)
4.  [employee](#employee)
5.  [company/ares/](#ares)
  
## company
#### GET
Response for `GET` request return all saved company sorted ascending alphabetically by their name in `json` format. 
Response contain information about companies and their branches. Employees and projects are not accessible through this request:
```json
[
    {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "name": "",
        "companyId": "",
        "vatId": "",
        "webUrl": "",
        "note": "",
        "active": true,
        "headquarters": {
            "city": "",
            "street": "",
            "houseNumber": "",
            "zipCode": "",
            "country": ""
        },
        "companyBranches": [
            {
                "id": 1,
                "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
                "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
                "name": "",
                "location": {
                    "city": "",
                    "street": "",
                    "houseNumber": "",
                    "zipCode": "",
                    "country": ""
                },
                "employees": null,
                "company": null
            },
            ...
        ],
        "projects": null
    },
    ...
]
```
#### POST
`POST` request saved new company into the database. Fields`name`, `companyId` must be unique.
Address values contained in `headquarters` must not be null.
Template for request in `json` format is:
```json
{
    "name": "",
    "companyId": "",
    "vatId": "",
    "webUrl": "",
    "note": "",
    "active": true,
    "headquarters": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": "",
        "country": ""
    }
}
```
Response for `POST` request return created company in `json` format with generated `id` and *timestamps*:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "name": "",
    "companyId": "",
    "vatId": "",
    "webUrl": "",
    "note": "",
    "active": true,
    "headquarters": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": "",
        "country": ""
    },
    "companyBranches": null,
    "projects": null
}
```
#### PUT
`PUT` request update company in the database. Request must contain company `id`, `name` and `companyId`. 
Template for request in ``json`` is:
```json
{
    "id": 1,
    "name": "",
    "companyId": "",
    "vatId": "",
    "webUrl": "",
    "note": "",
    "active": true,
    "headquarters": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": "",
        "country": ""
    }
}
```
Response for `PUT` request returned updated information in `json` format:
```json
{
    "id": 1,
    "createdAt": null,
    "updatedAt": null,
    "name": "",
    "companyId": "",
    "vatId": "",
    "webUrl": "",
    "note": "",
    "active": true,
    "headquarters": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": "",
        "country": ""
    },
    "companyBranches": null,
    "projects": null
}
```
#### DELETE
Request `DELETE` required `id` of company, that to be deleted as request parameter. 
## companybranch
#### GET
Requested parameter is `companyId` that represent primary key of company (`id`). 
Response for `GET` request return all branches of chosen company in `json` format. 
Response contain only information about company and their branches. 
Employees and projects are not accessible through this request: 
```json
[
    {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "name": "",
        "location": {
            "city": "",
            "street": "",
            "houseNumber": "",
            "zipCode": "",
            "country": ""
        },
        "employees": null,
        "company": {
            "id": 1,
            "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "name": "",
            "companyId": "",
            "vatId": "",
            "webUrl": "",
            "note": "",
            "active": true,
            "headquarters": {
                "city": "",
                "street": "",
                "houseNumber": "",
                "zipCode": "",
                "country": ""
            },
            "companyBranches": null,
            "projects": null
        },
        ...
    }
]
```
#### POST
`POST` request must contain `name` of new company branch, and address values contained in `location`.
 Field `company.id` represent company primary key, which created branch belong to.
 Template for request in `json` format is:
```json
{
    "name": "",
    "location": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": " ",
        "country": ""
    },
    "company": {
        "id": 1
    }
}
```
Response for `POST` request contain returned created company branch in `json` format with generated *timestamps* and `id`. 
Information about company, employees and projects are not included: 
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "name": "",
    "location": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": "",
        "country": ""
    },
    "employees": null,
    "company": {
        "id": 1,
        "createdAt": null,
        "updatedAt": null,
        "name": null,
        "companyId": null,
        "vatId": null,
        "webUrl": null,
        "note": null,
        "active": false,
        "headquarters": null,
        "companyBranches": null,
        "projects": null
    }
}
```
#### PUT
`PUT` request must contain `id` which is primary keys of entities. All fields must not be null. 
Template for `PUT` request in `json` format is:
```json
{
    "id": 1,
    "name": "",
    "location": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": " ",
        "country": ""
    },
    "company": {
        "id": 1
    }
}
```
Response for `PUT` request returned updated company branch values with values of company which owns that branch. 
Information about company branch employees and projects are not accessible through this request. 
Returned `json` format response is:
```json
{
    "id": 1,
    "createdAt": null,
    "updatedAt": null,
    "name": "",
    "location": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": " ",
        "country": ""
    },
    "employees": null,
    "company": {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "name": "",
        "companyId": "",
        "vatId": "",
        "webUrl": "",
        "note": "",
        "active": true,
        "headquarters": {
            "city": "",
            "street": "",
            "houseNumber": "",
            "zipCode": "",
            "country": ""
        },
        "companyBranches": null,
        "projects": null
    }
}
```
#### DELETE
Request `DELETE` must contain requested parameter `id` which is primary key of company branch that to be deleted.
## project
#### GET
1. Request without arguments
`GET` request returned as response list of all projects saved in database. 
Response contain company, that project belong to and list of employees assigned for this projects.
Response does not contain information about company branch.
2. Request with arguments
`GET` request also allows searching projects based on their name and time of creation. 
Requested parameters for this filter are `name` that contain substring of existing project name and `createdAfter` that filter projects based on their time of creation. 
Parameter `createdAfter` must be in ISO DateTime Format: **yyyy-MM-dd'T'HH:mm:ss.SSSXXX**

Response in `json` format for this requests is:
```json
[
    {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "name": "",
        "startDate": "",
        "endDate": "",
        "price": 1,
        "company": {
            "id": 1,
            "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "name": "",
            "companyId": "",
            "vatId": "",
            "webUrl": "",
            "note": "",
            "active": true,
            "headquarters": {
                "city": "",
                "street": "",
                "houseNumber": "",
                "zipCode": "",
                "country": ""
            },
            "companyBranches": null,
            "projects": null
        },
        "employees": [
            {
                "id": 1,
                "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
                "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
                "firstName": "",
                "surname": "",
                "salary": 1,
                "companyBranch": null,
                "projects": null
            },
            ...
        ]
    },
    ...    
]
```
#### POST
`POST` request must contain project `name`. Values of employees and company `id` field contains their primary key.
Template for this request in `json` format is: 
 ```json
{
    "name": "",
    "startDate": "",
    "endDate": "",
    "price": 1,
    "company": {
        "id": 1
    },
    "employees": [
        {
            "id": 1
        },
        ...
    ]
}
```
Response for `POST` request returned created project with generated *timestamp* and *primary key* in `json` format. 
Response contain information only about created project:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "name": "Table of sales creation",
    "startDate": "",
    "endDate": "",
    "price": 1,
    "company": {
        "id": 1,
        "createdAt": null,
        "updatedAt": null,
        "name": null,
        "companyId": null,
        "vatId": null,
        "webUrl": null,
        "note": null,
        "active": false,
        "headquarters": null,
        "companyBranches": null,
        "projects": null
    },
    "employees": [
        {
            "id": 1,
            "createdAt": null,
            "updatedAt": null,
            "firstName": null,
            "surname": null,
            "salary": 0,
            "companyBranch": null,
            "projects": null
        },
        ...
    ]
}
```
#### PUT
`PUT` request must contain *primary key* of project that to be updated and projects information. 
Changes of company that owns this project and assigned employee are accessible through this request by their `id`. 
Template for this request in `json` format is:
```json
{
    "id": 1,
    "name": "",
    "startDate": "",
    "endDate": "",
    "price": 1,
    "company": {
        "id": 1
    },
    "employees": [
        {
            "id": 1
        },
        ...
    ]
}
```
Response for `PUT` request contain updated information in `json` format. 
Information about company and employees are not included:
```json
{
    "id": 1,
    "createdAt": null,
    "updatedAt": null,
    "name": "",
    "startDate": "",
    "endDate": "",
    "price": 1,
    "company": {
        "id": 1,
        "createdAt": null,
        "updatedAt": null,
        "name": null,
        "companyId": null,
        "vatId": null,
        "webUrl": null,
        "note": null,
        "active": false,
        "headquarters": null,
        "companyBranches": null,
        "projects": null
    },
    "employees": [
        {
            "id": 1,
            "createdAt": null,
            "updatedAt": null,
            "firstName": null,
            "surname": null,
            "salary": 0,
            "companyBranch": null,
            "projects": null
        },
        ...
    ]
}
```
#### DELETE
Request `DELETE` must contain requested parameter `id` which is primary key of project that to be deleted.
## employee
#### GET
Request parameter for `GET` request is `companyId` which is primary key (`id`) of company. 
Response return list of all companies employees with company branch their belong to. 
Response does not return projects. Response for request in `json` format is:
```json
[
    {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "firstName": "",
        "surname": "",
        "salary": 1,
        "companyBranch": {
            "id": 1,
            "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "name": "",
            "location": {
                "city": "",
                "street": "",
                "houseNumber": "",
                "zipCode": "",
                "country": ""
            },
            "employees": null,
            "company": {
                "id": 1,
                "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
                "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
                "name": "",
                "companyId": "",
                "vatId": "",
                "webUrl": "",
                "note": "",
                "active": true,
                "headquarters": {
                    "city": "",
                    "street": "",
                    "houseNumber": "",
                    "zipCode": "",
                    "country": ""
                },
                "companyBranches": null,
                "projects": null
            }
        },
        "projects": null
    },
    ...
]
```
#### POST
`POST` request must contain `first name`, `surname`, and `salary` of new employee. 
Company branch `id` is a primary key of branch that new employee belong to.
Request in `json` format is:
```json
{
    "firstName": "",
    "surname": "",
    "salary": 1,
    "companyBranch": {
        "id": 1
    }
}
```
Response return new employee with generated *timestamp*s and *primary key*. 
Response in `json` format look like:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "firstName": "",
    "surname": "",
    "salary": 1,
    "companyBranch": {
        "id": 1
    },
    "projects": null
}
```
#### PUT
`PUT` request for employee update must contain `id`, `firstName`, `surname`, `salary` and company branch `id` that employee that to be updated belong. 
Request in `json` format is:
```json
{
    "id": 1,
    "firstName": "",
    "surname": "",
    "salary": 1,
    "companyBranch": {
        "id": 1
    }
}
```
Response in `json` format returned updated employee with company and company branch that updated employee belong to:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "firstName": "",
    "surname": "",
    "salary": 1,
    "companyBranch": {
        "id": 1,
        "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
        "name": "",
        "location": {
            "city": "",
            "street": "",
            "houseNumber": "",
            "zipCode": " ",
            "country": ""
        },
        "employees": null,
        "company": {
            "id": 1,
            "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
            "name": "",
            "companyId": "",
            "vatId": "",
            "webUrl": "",
            "note": "",
            "active": true,
            "headquarters": {
                "city": "",
                "street": "",
                "houseNumber": "",
                "zipCode": "",
                "country": ""
            },
            "companyBranches": null,
            "projects": null
        }
    },
    "projects": null
}
```

## company/ares/<a name="ares"></a>

#### GET
`GET` request for this endpoint mediates communication with [ARES](https://wwwinfo.mfcr.cz/ares/ares_es.html.cz) webservice. 
This request require `companyId` as path variable (`company/ares/{companyId}`).
Inserted `companyId` is send as request and returned response that is transformed into the new company. 
This company is automatically saved into the database. 
Request uses FREE option of `ARES` searching, for this reason returned response contain only basic information about company. 
Response does not contain `vatID` and `webUrl` field. Returned response in `json` format is:
```json
{
    "id": 1,
    "createdAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "updatedAt": "yyyy-MM-ddTHH:mm:ss.SSSXXX",
    "name": "",
    "companyId": "",
    "vatId": null,
    "webUrl": null,
    "note": null,
    "active": true,
    "headquarters": {
        "city": "",
        "street": "",
        "houseNumber": "",
        "zipCode": "",
        "country": ""
    },
    "companyBranches": null,
    "projects": null
}
```